# How to use Git

## A useful summary of commands

```python
mkdir getDir
cd gitDir
echo "Git is awesome" > gitText

git init
git add gitText
git commit -m "First Commit"

git log
git status
```

Once we have created a local git repository we might want to link this with one within Gitlab so that other developers can see our code. To do this we require:

```python
git remote add origin git@gitlab.com:mljames/playing-with-git.git
git remote -v
git push -u origin master
```

The golden rule is to ** commit early and commit often **. 

To remove 'repository status' simply go to the head of the system and delete all .git files so something like:

```python
 rm -rf .git
```

Further to this one must remove the repository on Gitlab too.

So far we have only discussed adding and commiting to a master branch. One can branch from this master branch tree.

```python
git checkout -b 'branch'
```

This new branch will contain a copy of the files from the branch from which it has branched (which could be the master)

To switch back to the master branch we simply invoke:

```python
git checkout master
```

To merge a branch to a master one must:

```python
git checkout master
git merge branch
```

This final command may produce a conflict when we merge the two branches if both branches modify the same line of the same file. Alternatively we can actually go to gitlab and submit a pull request. From this request people may review it and finally merge it. This requires however updating the local repository. To see which branches exist locally we type:

```python
git branch -a
```

This prints lines such as remotes/orange/beef or remotes/orange/master which are hidden remotes, that we require in order to find which merges to do within our local repository.

```python
git fetch
```

Then we employ:

```python
git merge orange/master
```

The reverse, i.e. telling the remote brange we've made a change, such as a deletion:

```python
git branch -D beef
git branch
git branch -a
git push -d orange beef
```

HEAD in git refers simply to the current branch. When we switch branches with "git checkout" the HEAD revision changes to point to the tip of the new branch. We can see what HEAD points to by doing:

```python
cat .git/HEAD
```

It is therefore possible to reset the current HEAD to a specified state:

```python
git reset
git reset --hard
```

The "hard" option resets the index and working tree and any changes to tracked files in the working tree since <commit> are discarded. Other options include "--soft", "--mixed" and "--merge". "git diff" may be used to show changes between commits, commit and *working*  *tree* , etc.

If we add a gitignore after some files have already been added to the repository then we need to run the following commands:

```bash
git rm -r --cached .
git add .
git commit -m ".gitignore is now working"
```