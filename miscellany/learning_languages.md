# Learning a Computer Language

Software engineers should pick the right language - if they don't they will introduce a lot of complexity to their code.

What defines a programming language?

* Paradigm - classification of languages by their features
  * Imperative: define how to compute
  * Declarative: define constraints

* Execution Model - how you go from human-readable code to running on a machine
  * Interpreted: parse and execute line by line
  * Compiled: convert source into native code once, then execute
  * Just-in-time compiled: this is a combination of interpreted and compiled e.g. Java

* Syntax - the textual structure of a valid program
  * Compilers will reject syntactically-invalid code

* Semantics - the meaning of the program
  * The set of rules that define the meaning of syntactic constructs

* Type System - how the language prevents incorrect operations
  * Data types constrain the values that variables can take
  * Type systems specify how data types are defined and how they can be used - this discourages type errors
  * Type enforcement can occur at compile time (static) or runtime (dynamic) or both
  * "A type-safe language may have trapped errors [one that can be handled gracefully], but can't have un-trapped errors [ones that cause unpredictable crashes]
