# Testing

### Types of test

* Unit - in memory
* Integration - against some other system
* System - multiple systems against each other

### Good software design takes the pain out of testing

* Single responsibility principle => lots of layers
* Inject dependencies into classes
* Avoid global variables
* D.R.Y

