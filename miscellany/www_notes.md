# The World Wide Web

## Jargon

* URLs - <protocol>://<host><:optional port>/<path/to/resource><?query> e.g. http://localhost:8080/mainpage?q=param1&q2=param2

* HTTP is one of the protocols - it is *connectionless*, *stateless* and *media type agnostic*.

* Client - is the requester

* Server - acts upon a request, sends back to the client. We need to be able to find where the server is - we need its name! A DNS server takes `www.thg.com` and turns it into a web address - there are only 900 root DNS servers around the world. The root server will see `www.thg.com` and say I don't know the address but a `.com` server might. The same process happens for `thg.com` and thus we find `www.thg.com`. We then get an IP address back which we can store in a cache, so this lookup doesn't have to be repeated.

A HTTP request contains a Verb (for example `GET`, `PUSH`, `FETCH` etc.), a URI (for example the most common are URLs) and a Version. It will also have a set of request headers and a request message.

* GET is a request for a representation of a particular resource. GETs are therefore cascheable. A cacheable response is an HTTP response that can be cached, that is stored to be retrieved and used later, saving a new request to the server.

* POST is a request data often relating to a form post

Possible status codes include:

* 2xx - OK (No Error): 200 OK, 204 No Content

* 3xx - Redirect to different URL: 301 Moved Permanently, 302 Moved Temporarily

* 4xx - Client Error (browser's done something wrong): 400 Bad Request, 403 Forbidden, 404 Not Found, 418 I'm a teapot

* 5xx - Internal Server Error: 500 Internal Servor Error, 503 Service Unavailable

## TLS and HTTPS (Transport Layer Security)

Assymetric encryption requires a public and a private key. The server takes data, signs it with a private key and sends it to the client who can decrypt it. This uses the RSA algorithm. To make a connection with a server, the process might follow the steps:

* A client tries to connect with a TLS enabled server requesting a secure connection, presenting a list of supported cipher suites.
* The server picks a cipher and hash function that it also supports and notifies the client of the decision.
* The server provides identification in the form of a digital certificate.
* We trust the certifacte authorities.
* They issue a certificate with a server's nameand public key when the client requests from a server.
* The client uses this public key to encrypt a one time only authorisation key, perhaps derived from a random number.
* The server with the correct private key can decrypt the original message.
* Both the server and the client then use the random number to generate a unique sesion key for subsequent excryption and decryption.
* The handshake is concluded and a secure connection between the server and the client can be established.

*We may also use *Diffie-Hellman key exchange* to securely generate a random and unique session key for excryption and decryption, which has an additional property of forward secrecy.*

* Further communication is encrypted using a symetric cipher once the shared key is known by both client and server.

**Extra resources:** *https://en.wikipedia.org/wiki/Transport_Layer_Security*

