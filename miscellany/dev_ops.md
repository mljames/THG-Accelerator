# DevOps

## Where does my code run?

DevOps is a software development methodology that combines software development with information technology operations. The goal of DevOps is to shorten the systems development life cycle while also delivering features, fixes and updates freqeuently in close alignment with business objectives.

The layers of infrastructure for running code follows:

Facilities -> Network -> Storage -> Servers -> Infrastructure Management Tools and Services -> Applications

The tool chain for DevOps follows the schema:

1. Code - Software Engineering
2. Build - Continuous Integration
3. Test - Continuos Testing
4. Package - Asset creation (packages to deploy)
5. Release - Change Management etc.
6. Configure - Infrastructure configuration and management
7. Monitor - APM, end-user experience

Further resources: The Pheonix Project by Gene Kim, Kevin Behr and George Spafford