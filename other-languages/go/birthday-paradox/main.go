package main

// A birthday is just one of the 366 days in the year

import (
  "fmt"
  "math/rand"
  "time"
  "os"
  "strconv"
)

func main() {
  x,_ := strconv.Atoi(os.Args[1])
  count := 0
  for i := 0; i < 100000; i++ {
    rand.Seed(time.Now().UTC().UnixNano())
    birthdays := randomonise(x)
    if len(birthdays) < x {
      count = count + 1
    }
  }
  result := float64(count)/float64(1000)
  fmt.Println("The probability that two people share the same birthday in a room of ",x," people is ",result, "percent")
}

func randomonise(x int) map[int]bool {
  slice := make(map[int]bool)
  for i := 0; i < x; i++ {
    slice[rand.Intn(366)] = true
  }
  return slice
}
