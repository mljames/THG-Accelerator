// Building a compiler in go

package main

import (
  "fmt"
  "bufio"
  "os"
  "unicode"
  "strconv"
)

// Helper function to assert whether an input string is an integer

func isInt(s string) bool {
    for _, c := range s {
        if !unicode.IsDigit(c) {
            return false
        }
    }
    return true
}

// Structs required for lexer and parser

type token struct {
  token_type string
  token_value string
}

type interpreter struct {
  text string
  pointer_position int
  current_character string
}

// main function

func main() {
  reader := bufio.NewReader(os.Stdin)
  for {
  fmt.Print("calc> ")
  calc, _ := reader.ReadString('\n')
  calculation := interpreter{text:calc,pointer_position:0,current_character:""}
  ticket := token{token_type:"None",token_value:"None"}
  calculation.current_character = string(calculation.text[0])
  get_next_token(&ticket, &calculation)
  result := expr(&ticket, &calculation)
  fmt.Println(result)
}
}

// functions for the lexer

func advance(calculation *interpreter) {
  calculation.pointer_position += 1
  length := len(calculation.text)
  if calculation.pointer_position > length - 2 {
    calculation.current_character = "None"
  } else { calculation.current_character = string(calculation.text[calculation.pointer_position]) }
}

func whitespace(calculation *interpreter) {
  for calculation.current_character != "None" && calculation.current_character == " " {
    advance(calculation)
  }
}

func integer(calculation *interpreter) string {
  result := ""
  for calculation.current_character != "None" && isInt(calculation.current_character)  {
    result += calculation.current_character
    advance(calculation)
  }
  return result
}

func error() {
  fmt.Println("Error With Input!!")
  os.Exit(1)
}

func get_next_token(tokenise *token, calculation *interpreter) {
  for calculation.current_character != "None" {
    whitespace(calculation)
    if isInt(calculation.current_character) == true {
        tokenise.token_type = "INTEGER"
        tokenise.token_value = integer(calculation)
        return
      } else if calculation.current_character == "+" {
        advance(calculation)
        tokenise.token_type = "PLUS"
        tokenise.token_value = "+"
        return
      } else if calculation.current_character == "-" {
        advance(calculation)
        tokenise.token_type = "MINUS"
        tokenise.token_value = "-"
        return
      } else if calculation.current_character == "*" {
        advance(calculation)
        tokenise.token_type = "TIMES"
        tokenise.token_value = "*"
        return
      } else if calculation.current_character == "/" {
        advance(calculation)
        tokenise.token_type = "DIVIDE"
        tokenise.token_value = "/"
        return
      } else if calculation.current_character == "(" {
        advance(calculation)
        tokenise.token_type = "LPARA"
        tokenise.token_value = "("
        return
      } else if calculation.current_character == ")" {
        advance(calculation)
        tokenise.token_type = "RPARA"
        tokenise.token_value = ")"
        return
      } else {
        error()
        tokenise.token_type = "EOF"
        tokenise.token_value = "None"
        return
      }
    }
}

// functions for parser

func eat(tokenise *token, calculation *interpreter, expected_token string) {
  if tokenise.token_type == expected_token {
    get_next_token(tokenise, calculation)
  }
}

func factor(tokenise *token, calculation *interpreter) int {
  result, _ := strconv.Atoi(tokenise.token_value)
  if tokenise.token_type == "INTEGER" {
  eat(tokenise, calculation, "INTEGER")
  return result
  } else if tokenise.token_type == "LPARA" {
    eat(tokenise, calculation, "LPARA")
    result = expr(tokenise, calculation)
    eat(tokenise, calculation, "RPARA")
    return result
} else { error() }
return 0
}

func term(tokenise *token, calculation *interpreter) int {
  result := factor(tokenise, calculation)
  for tokenise.token_type == "TIMES" || tokenise.token_type == "DIVIDE" {
    if tokenise.token_type == "TIMES" {
      eat(tokenise, calculation, "TIMES")
      result = result * factor(tokenise, calculation) }
    if tokenise.token_type == "DIVIDE" {
      eat(tokenise, calculation, "DIVIDE")
      result = result / factor(tokenise, calculation) }
    }
    return result
  }
func expr(tokenise *token, calculation *interpreter) int {
  result := term(tokenise, calculation)
  for tokenise.token_type == "PLUS" || tokenise.token_type == "MINUS" {
    if tokenise.token_type == "PLUS" {
      eat(tokenise, calculation, "PLUS")
      result += term(tokenise, calculation)
    }
    if tokenise.token_type == "MINUS" {
      eat(tokenise, calculation, "MINUS")
      result -= term(tokenise, calculation) }
  }
  return result
}
