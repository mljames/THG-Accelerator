package main

import (
  "fmt"
  "math/rand"
  "time"
)
// The first prisoner will turn the light from off to on, we therefore
// begin the light switch as on and note the first_inmate
// has already switched the light on. If a prisoner arrives in the room
// with the light switch on and it is the first time it has been on in the times
// that they have entered then they will turn it off, but otherwise do
// nothing. The first prisoner will switch the light on whenever it is off and count
// one. Once the first prisoner has turned the light on 100 times he will be 100%
// certain that all the inmates have been in the room

func main () {
  number_of_prisoners := 100
  rand.Seed(time.Now().UTC().UnixNano())
  first_inmate := rand.Intn(number_of_prisoners)
  rand.Seed(time.Now().UTC().UnixNano())
  prisoners := make(map[int]bool)
  switch_state := true
  for i := 0; i < number_of_prisoners; i++ {
    prisoners[i] = false
  }
  prisoners[first_inmate] = true
  days := 0
  first_prisoner_count := 1
  for first_prisoner_count < number_of_prisoners {
    selected := rand.Intn(number_of_prisoners)
    switch_state, first_prisoner_count = light_switch(switch_state, prisoners[selected], selected, first_prisoner_count, first_inmate)
    days = days + 1
    fmt.Println("Number of days -",days, "--- Number of times first prisoner -",first_prisoner_count, "--- Current prisoner-",selected)
  }
  fmt.Println("It takes",days, "days for prisoner", first_inmate, "to be 100% certain everyone has entered the room")
}

func light_switch(s bool, b bool, selected int, first_prisoner_count int, first_inmate int) (bool,int) {
  switch switch_state := s; switch_state {
  case true:
    if selected != first_inmate {
      if b == true {
        return true, first_prisoner_count
      }
      return false, first_prisoner_count
    } else { return true, first_prisoner_count }
  case false:
    if selected != first_inmate {
      return false, first_prisoner_count
    } else {
      first_prisoner_count = first_prisoner_count + 1
      return true, first_prisoner_count
            }
    }
    return false, first_prisoner_count
}
