package main

import (
  "fmt"
  "time"
  "math/rand"
)

func main() {
  count := 0
  counting_all := 0
  for i := 0; i < 100000; i++ {
    rand.Seed(time.Now().UTC().UnixNano())
    boxes := create_boxes()
    select_box := select_box(boxes)
    select_coin := select_coin(select_box)
    if select_coin == "Gold" {
      counting_all = counting_all + 1
    }
    count = gold(count, select_box, select_coin)
  }
  percentage := fmt.Sprintf("%.2f",float64(count)*100/float64(counting_all))
  fmt.Println("The chances of picking gold from the same box you've picked gold from is ",percentage, "percent which seems counter-intuitive?!")
}

type box struct {
  BoxName string
  OneCoin string
  AnotherCoin string
}

func create_boxes() [3]box {

  box1 := box{BoxName:"A",OneCoin:"Gold",AnotherCoin:"Gold"}
  box2 := box{BoxName:"B",OneCoin:"Gold",AnotherCoin:"Silver"}
  box3 := box{BoxName:"C",OneCoin:"Silver",AnotherCoin:"Silver"}
  boxes := [3]box{box1,box2,box3}

  return boxes
}

func select_box(boxes [3]box) box {
  return boxes[rand.Intn(3)]
}

func select_coin(box box) string {
  coin_pick := rand.Intn(2)
  if coin_pick == 0 {
    return box.OneCoin
  }
  return box.AnotherCoin
}

func gold(count int, box box, coin string) int {
  if coin == "Gold" {
    if box.AnotherCoin == "Gold" && box.OneCoin == "Gold" {
      return count + 1
    } else { return count}
  } else { return count }
}
