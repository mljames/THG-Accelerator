package main
import "fmt"

// How are struts different to maps?
// Maps - all keys must be the same type, all values must be the same type, the
//keys are indexed so we can iterate over them, we can use maps to represent a
//collection of related properties, we don't need to know all the keys at compile
//time and they're reference type
// Structs - values can be of different type, the keys don't support indecing
// we need to know all the different fields at compile time
// All the keys are of type string and all the values are of type string often
// used to represent a "thing" with a lot of different properties and they're
// value type

func main() {
  //var colors map[string]string
  //colors := make(map[int]string)
  colors := map[string]string{
    "red":"#ff0000",
    "green":"#4bf745",
    }
  printMap(colors)
}

func printMap(c map[string]string) {
  for color, hex := range c {
    fmt.Println("Hex code for", color, "is", hex)
  }
}
