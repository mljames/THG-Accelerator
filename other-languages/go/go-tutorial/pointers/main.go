package main

import "fmt"

func main() {
  y := 0
  fmt.Println("Value",y," address",&y)
  change(y)
  fmt.Println("Value",y," address",&y)
  changeypointer(&y)
  fmt.Println("Value",y," address",&y)
}

func change(y int) (int,*int){
  y =  3
  return y, &y
}

func changeypointer(y *int) (int, *int) {
     *y = 10
 return *y, y
}