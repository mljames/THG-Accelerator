package main

// Every value has a type and every function has to specify the type of its
// arguments - does this mean that every function we ever write has to be rewritten
// to accomodate different types even if the logic is identical?

import (
  "fmt"
  "os"
  "net/http"
  "io"
)

type logWriter struct{}

func main() {
  resp, err := http.Get("http://google.com")
  if err != nil {
    fmt.Println("Error:",err)
    os.Exit(1)
  }
  //bs := make([]byte,99999)
  //resp.Body.Read(bs)
  //fmt.Println(string(bs))
  lw := logWriter{}
  io.Copy(lw, resp.Body)
}

func (logWriter) Write(bs []byte) (int, error) {
  fmt.Println(string(bs))
  fmt.Println("Just wrote this many bytes:", len(bs))
  return len(bs), nil
}
