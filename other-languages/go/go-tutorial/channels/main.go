package main

// One CPU Core  -> Go Scheduler -> Go Routine, Go Routine, Go Routine
// where the scheduler runs one thread on each "logical" core and works
// very hard in the background to switch which Go routine runs at any time
// depending on blocking functions etc.
// Concurrency - we can have multiple threads executing code - if one thread
// blocks another one is picked up and worked on
// Parrallelism - multiple threads executed at the same time - this requires
// multiple CPUs
// Function literal in go is an unnamed function - lambda function

import (
  "fmt"
  "net/http"
  "time"
)

func main() {
  links := []string{
    "http://google.com",
    "http://facebook.com",
    "http://stackoverflow.com",
    "http://golang.org",
    "http://amazon.com",
  }

  c := make(chan string)

  for _, link := range links {
    go checkLink(link, c)

  }
  for l := range c {
    go func(link string) {
      time.Sleep(5*time.Second)
      checkLink(link,c)
    }(l)
  }
}

func checkLink(link string, c chan string) {
  _,err := http.Get(link)
  if err != nil {
    fmt.Println(link,"might be down!")
    c <- link
    return
  }
  fmt.Println(link,"is up!")
  c <- link
}
