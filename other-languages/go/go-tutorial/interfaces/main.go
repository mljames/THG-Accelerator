package main

import "fmt"

// Third custom type called bot - if you are type with a function
// called getGreeting which returns a type string then you are an honorary
// member of type bot
// Now that you're an honrary member of type bot you can now call this function
// called 'printGreeting'

type bot interface {
  getGreeting() string
}

type englishBot struct{}
type spanishBot struct{}

func main() {
  eb := englishBot{}
  sb := spanishBot{}

  printGreeting(eb)
  printGreeting(sb)
}

func printGreeting(b bot){
  fmt.Println(b.getGreeting())
}

// printGreeting(eb englishBot) {
//fmt.Println(eb.getGreeting())
//}
//func printGreeting(sb spanishbot) {
  //fmt.Println(sb.getGreeting())
//}

func (eb englishBot) getGreeting() string{
  // VERY custom logic for generating an English getGreeting
  return "Hi There"
}

func (sb spanishBot) getGreeting() string{
  return "Hola!"
}
