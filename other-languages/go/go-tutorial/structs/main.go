// Value Types - int, float, string, bool, structs
// Reference Types - slices, maps, channels, pointers, functions

package main

import "fmt"

type contactInfo struct {
  email string
  zipCode int
}

type person struct {
  firstName string
  lastName string
  contactInfo
}

func main() {
  jim := person{
    firstName: "Jim",
    lastName: "Party",
    contactInfo: contactInfo{
      email:"jim@gmail.com",
      zipCode: 94000,
    },
  }
  // &variable - give me the memory address of the value this variable is pointing to
  // *pointer - give me the value this memory address is pointing at
  // *person where person is a type - this function call only works when we receive a type which is a pointer to a person
  // *pointerToPerson where pointerToPerson is an operator - we want to manipulate the value the pointer is referencing

  jim.updateName("jimmy")
  jim.print()
}

func (pointerToPerson *person) updateName(newFirstName string) {
  (*pointerToPerson).firstName = newFirstName
}

func (p person) print() {
  fmt.Printf("%+v",p)
}
