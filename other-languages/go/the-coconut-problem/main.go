package main

import (
  "fmt"
)

func main() {
  coconut_init := 5
  num_sailors := 5
  for true  {
    coconut_counter := coconut_init
    sailor_count := 0
    for i := 0; i < num_sailors; i++ {
      if wake(num_sailors, coconut_counter) {
        coconut_counter = divide(num_sailors, coconut_counter)
        sailor_count += 1
        continue
      } else { break } }
    if morning(num_sailors, coconut_counter) && sailor_count == num_sailors {
      fmt.Println("The minimum possible size of the intial pile of coconuts collected during the first day is", coconut_init)
      break
    }
    coconut_init += 1
  }
}

func wake(num_sailors int, num int) bool {
  if num%num_sailors == 1 {
    return true } else { return false }
}

func morning(num_sailors int, num int) bool {
  if num%num_sailors == 0 && num >= num_sailors {
    return true } else { return false }
}

func divide(num_sailors int, num int) int {
  share_hidden := int((num-1)/num_sailors)
  coconuts_returned := num - share_hidden - 1
  return coconuts_returned
}
