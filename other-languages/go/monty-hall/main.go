package main

import (
  "fmt"
  "math/rand"
  "time"
)

func main() {
  rand.Seed(time.Now().UTC().UnixNano())
  simulations := 1000000
  swap := true
  correct_1 := 0
  for i := 0; i < simulations; i++ {
    if simulation(swap) == true {
      correct_1 = correct_1 + 1
    }
  }
  swap = false
  correct_2 := 0
  for i := 0; i < simulations; i++ {
    if simulation(swap) == true {
      correct_2 = correct_2 + 1
    }
  }
  result := float64(correct_1) / float64(correct_2)
  fmt.Println(correct_1, correct_2)
  fmt.Println("You are",result,"times more likely to win if you swap")
}

func simulation(s bool) bool {
  doors := []int{0, 0, 0}
  winning_door := rand.Intn(3)
  doors[winning_door] = 1
  selected := rand.Intn(3)
  switch swap := s; swap {
  // player switches
  case true:
    if doors[selected] == 0 {
      return true
    }
      return false
  // player does not switch
  case false:
    if doors[selected] == 0 {
      return false
    }
    return true
}
return true
}
