# JavaScript Essentials

### Values and Types

```javascript
typeof 3 === 'number'
typeof 3.1 ==== 'number'
typeof (() => {}) === 'object'
typeof 'hello world' === 'string'
typeof true === 'boolean'
```

### Variable

```javascript
const x = 3 + 3;
let y = 'hello world';

const xs = [];
xs.push(3);
```

If we use const and let it will not be available in the global - this is a good thing.

### Function Expression

```javascript
/*
const h = function () {
      return f(7, 5);
};
const h = () => {
      return f(7, 5);
};
*/
const h = () => f(7, 5)
const f = function(x, y) {
      return x + y;
};
f(5, 7)
h()
```

### Function Declaration

```javascript
g(1, 2)
function g (z, k) {
	 return z - k;
};
```

Function declarations are put at the top of the script and are defined globally. We can therefore call g(1,2) in a line before that function is declared in the script.

### Conditionals

** `===` checks the type as well since otherwise `1 == "1"` would evalute to true **

```javascript
if (1 + 1 === 2) {
   const x = 1;
} else if (2 - 1 < 3) {
   const y = 2;
} else {
}

const x = 'hello world';
switch (x) {
    case 'hellow world':
        // lines
	break;
    default:
        // lines
	break;
}
```

### Global

### Loops

A low level for-loop:

```javascript
for (let i = 0; i < 10; i++) {
    console.log('Hello World!')
}

const zs = ['mary', 'jam', 'butter'];
for (let i=0; i < zs.length; i++) {
    console.log(i, zs[i]);
}
```

A for-in loop:

```javascript
const person = {
    name: 'Jason Yu',
    age: 23,
    work: 'thg',
};

const jobless = !('work in person);
for (const key in person) {
    console.log(key, person[key]);
}

const xs = ['a', 'b', 'c'];
for (const key in xs) {
    console.log(key, xs[key]);
}
```

for-of loop

```javascript
const ys = ['mary', 'jam', 'butter'];
for (const y of ys) {
    console.log(y);
}
```

** Exercise **

Factorial (exercise)

```javascript
const fastRec = (n) => {
    if (n === 0) return 1;
    return n * factRec(n-1);
};
console.log(fastRec(5))
const factLoop = (n) => {
   for (let i = 1; i <= n; i++) {
       ans = ans * i;
   }
   return ans
};
console.log(factLoop(10))
const fastFanct = (n) => {
   return Array.from({length: n}).map((x,i) => i+1).reduce((x,y) => x*y);
};
console.log(factFancy(20))
```

#### Asynchronous Operations

A promise is an object that represents an asynchronous operation. A callback is a function that is to be executed after another function has finished executing. Promises avoid 'call-back hell'.

```javascript
var promise1 = Promise.resolve(3);
var promise2 = 42;
var promise3 = new Promise(function(resolve, reject) {
  setTimeout(resolve, 100, 'foo');
});

Promise.all([promise1, promise2, promise3]).then(function(values) {
  console.log(values);
});
// expected output: Array [3, 42, "foo"]
```

The `then()` method returns a Promise. It takes up to two arguments: callback functions for the success and failure cases of the Promise.

```javascript
var promise1 = new Promise(function(resolve, reject) {
  resolve('Success!');
});

promise1.then(function(value) {
  console.log(value);
  // expected output: "Success!"
});
```

A good resource on callbacks can be found https://codeburst.io/javascript-what-the-heck-is-a-callback-aba4da2deced.