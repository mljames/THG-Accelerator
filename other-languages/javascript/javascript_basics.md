# Javascript Introduction

* JavaScript has nothing to do with Java - it used to be called LiveScript.

* JavaScript has its own specification called ECMAscript.

* JavaScript was initially created to make 'web pages alive'.

There are two sides, client and server. Clients include Chrome, Opera, Firefox, Safari etc. On the server side we have NodeJS.

### Data types

* Number - 64 but double precision format
* String
* Boolean
* Null
* Undefined
* Object
* Symbol

### Functions

```javascript
function name(parameters, delimited, by, comma) {
  / * code * /
  // by default return undefined;
}
```

Below are a couple of examples of how a simple function may be implemented:

```javascript
const sum = (a,b) => a + b;
let sum = function(a,b) {
  return a + b
}
```

A function is an action, so function names are usually verbal - create.., show.., get.., check.. etc. A name should clearly describe what the function does, e.g. `printPage(page)`, `printText(text)` etc.

###### First class functions

```JavaScript
const callback = () => 5;
const example = function(fn) {
  fn();
};
example(callback)
```

###### Pure and impure functions

Favour pure functions in development - side effects may involve:

* Mutations
* Accessing system state
* Obtaining user input

Can use impure functions but must use a monad which acts as a container that abstracts away side effects. A monad is a way of composing functions that require context in addition to the return value.

```JavaScript
const xs = [1,2,3,4,5];

xs.slice(0,3); // pure
xs.splice(0,3); // impure
```

### Data Structures

###### Array (Stacks and Queues)

```javascript
let fruits = ["Apple","Orange","Pear"];
```

We can pop and push from stacks, which are fast operations. Unshift and shift are much much slower since they require shifting each and every index along one within the queue.

###### Hash tables

```javascript
let user = { name : 'John', age : 30 };
```
