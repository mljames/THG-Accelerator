# CSS Crash Course

## An Overview of a few things..

CSS stands for Cascading Stylesheets. It is **NOT** a programming language, it a Stylesheet or Styling language. It is used for website layout and design.

What we need to start:

* Safari
* Emacs

There are 3 methods for adding CSS to a html element:

* **Inline CSS:** Directly in the html element - an absolute no
* **Internal CSS:** Using `<style>` tags within a singe document
* **External CSS:** Linking an external.css file

The external method is suggested. This would be implemented by the line:

```html
<!DOCTYPE html>
<html>
<head>
    <title>CSS Cheat Sheet</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
</body>
</html>
```

The basic CSS selector follows the structure:

```html
a { background-color: yellow; }
```

The pipeline is selector, declaration start, property, property/value separator, value, declaration separator and declaration end. For example, for the `<body>` tag:

```css
body{
    background-color:#f4f4f4;
    color:#555555;
    }
```

For colors in CSS we can use *color names* (color: red;), *HTML5 color names* (color:coral;), *hexadecial values* (color: #00ff00;) and *RGB* (color: rgb(0,0,255)).

The difference between id and class - ids should always be unique, but we can always reuse classes.

### Box model

* Content
* Padding
* Border
* Margin

Hierarchy of space outside an element called content.

```css
p{
    margin: 5px 10px 5px 10px;
}
```

Where the order is top, right, bottom, left.

### Positioning in CSS

* Static
* Relative
* Absolute
* Fixed
* Initial
* Inherit

### Responsiveness

We can also make our website repsonsive to say the browser window increasing or decreasing in width by:

```css
@media(max-width:600px){
    #main{
        width:100%;
	float:none;
    }

    #sidebar{
        width:100%;
	float:none;
    }
}
```

### Some selector tricks

* `div {...}`  - selects all <p> elements if they exist 
* `div > p {...}` - selects only the next <p> element - direct child
* `h2 ~ p {...}` - selects all <p> on same 'level' as <h2>
* `h2 + p {...}` - selects the next <p> element on same 'level' as <h2>
* `a[href$=".pdf"] {...}` - $ sign acts a search term, searching all the attributes <a>

## Example Code

Below follows html and CSS code from the video tutorial.

First the html code to se up the core outline of the website:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>CSS Cheat Sheet</title>
    <link rel="stylesheet" type="text/css" href="CSS/style.css">
  </head>
  <body>
    <div class="container">
        <div class="box-1">
            <h1>Hello World</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div>
	        <div class="box-2">                                                                                               
            <h1>Goodbye World</h1>                                                                                          
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	    <a class="button" href="">Read More</a>
		</div>
		<div class="categories">
		  <h2>Categories</h2>
		  <ul>
		    <li><a href="#">Category1</a></li>
		    <li><a href="#">Category2</a></li>
		  </ul>
		</div>
		<form class="my-form">>
		  <div class="form-group">
		    <label>Name: </label>
		    <input type="text" name="name">
		  </div>
		  <div class="form-group">
		    <label>Email: </label>
		    <input type="text" name="email">
		  </div>
		  <div class="form-group">
		    <label>Message: </label>
		    <textarea name="message"></textarea>
		  </div>
		  <input class="button" type="submit" value="Submit">
		</form>
		<div class="block">
		  <h3>Heading</h3>
		  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
		</div>
		<div class="block">
		  <h3>Heading</h3>
		  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
		</div>
		<div class="clr"></div>
		<div id="main-block">
		  <h3>Heading</h3>
		  <p>Lorem ipsum doloe sit amet, consectetur adipisiing elit, deief efeinw wdinwdin wdinwdinwdc</p>
		  </div>
		  <div id="sidebar">
		    <p>Lorehbh sdhbsdcibsd dsubcsdbcd dsbcsdibcd  dsiubsdiubcsd  sdciusdci dsc sdidsibcsd dsibdsibcdis</p>
		  </div>
		  <div class="clr"></div>
		  <div class="p-box">
		    <h1>Hello</h1>
		    <h2>Goodbye</h2>
		  </div>
		  <ul class="my-list">>
		    <li>List Item</li>
		    <li>List Item</li>
		    <li>List Item</li>
		    <li>List Item</li>
		    <li>List Item</li>
		    <li>List Item</li>
</ul>
    </div><!-- ./container -->
<a class="fix-me button" href="">hello</a>
  </body>
  </html>
```

And secondly the CSS script which links with the html document to provide specific styling and fine tuning of the website:

```css
/*
*{
    margin:0;
    padding:0;
}
*/

/* global attributes for hyperlink tag */

a{
    text-decoration:none;
    color:#000;
}

a:hover{
    color:red;
}

a:active{
    color:green;
}

a:visited{
}

.button{                                                                      
    background-color:#333;                                                                
    color:#fff;                                                                                     
    padding:10px 15px;                                                                              
    border;none;                                                                                    
}             

.button:hover{
    background:red;
    color:white;
}

.clr{
    clear:both;
}
body{
    background-color:#f4f4f4;
    color: #555555;

    font-family: Arial Black, Helvetica;
    font-size: 12px;
    font-weight:bold;
    /* Same as above */
    font: bold 12px Arial, Helvetica, sans-serif;

    line-height:1.6em;
    margin:0; /* Headings have margin by defalt */
}

/* To target a class we use a dot */

.container{
    /* width:500px; - means that it is 500 pixels wide but unresponsive however */
    width:80%;
    margin:auto; /* Sets an auto margin on all sides - it puts an even amount of margin on either side */

}
.box-1{
    background-color:#333333;
    color:#fff; /* Text color */

    border-right: 5px red solid; /* Size color style */
    border-left: 5px red solid;
    border-top: 5px red solid;
    border-bottom: 5px red solid;

    /* Same as above */
    border: 5px red solid;
    border-width: 3px;
    border-bottom-width: 10px;
    border-top-style: dotted; /* solid dotted dashed doubled etc. */

    padding-top:20px;
    padding-bottom:20px;
    padding-right:20px;
    padding-left:20px;

    /* Same as above */
    padding: 20px;

    margin-top:20px;
    margin:20px 0;
}

/* This targets any h1 which is in this class */

.box-1 h1{
    font-family:Tahoma;
    font-weight:800;
    font-style:italic;
    text-decoration:underline;
    text-transform:uppercase;
    letter-spacing: 0.2em;
    word-spacing:1em;
}

.box-2{
    border:3px dotted #ccc;
    padding:20px;
    margin:20px 0;
}

.categories{
    border:1px #ccc solid;
    padding:10px;
    border-radius:15px;
}

.categories h2{
    text-align: center;
}

/* <ul > defines an unodrdered bulleted list */
/* <a > defines a hÂyperlink with most important attribute href= - a global variable defined at the top */
/* <li > specifies which kind of bullet point will be used be it value, number etc. */

.categories ul{
    padding:0;
    list-style:square;

}

.categories li{
    padding-bottom:6px;
    border-bottom:dotted 1px #333;
    padding-left:20px;
    /* list-style-image: url('../images/check.png') */
}

.my-form{
    padding:20px;
}

.my-form .form-group{
    padding-bottom:15px;
}

.my-form label{
    display:block
}

.my-form input[type="text"], .my-form textarea{
    padding:8px;
    width:100%;
}

/* Want to do this to everything of type button so in fact send this to the top of css script with tag .button 

.my-form input[type="submit"]{
    background-color:#333;
    color:#fff;
    padding:10px 15px;
    border;none;
}

*/

.block{
    float:left; /* float property is used for positioning and layout on web pages */
    width:50.0%;
    border:1px solid #ccc;
    padding:10px;
    box-sizing:border-box;
}

/* use # because these are ids not classes */

#main-block{
    float:left;
    width:70%;
    padding:15px;
}

#sidebar{
    float:right;
    width:30%;
    background-color:#333;
    color:#fff
    padding:15px;
    box-sizing: border-box;
}

.p-box{
    width:800px;
    height:500px;
    border:1px solid #000;
    margin-top:30px;
    position:relative;
    background-image:url('../images/image.jpeg');
    background-position:100px 200px;
    background-repeat:no-repeat;
    background-position:center top;
}

.p-box h1{
    position:absolute;
    top:100px;
    left:200px;
}

.p-box h2{
    position:absolute;
    bottom:40px;
    right:100px;
}

.fix-me{
    position:fixed;
    top:400px;
}

.my-list li:first-child{
    background-color:red;
}

.my-list li:last-child{
    background-color:blue;
}
```