## Data Science Lecture Notes

* "Data Science is a blend of various tools, algorithms, and machine learning principle with the goal to discover hidden patterns from the raw data"

The Data Science workflow is:

* Ask an interesting question
* Get the data
* Explore the data
* Model the data
* Communicate and visualise the results
* Loop to top

### Data Preparation

* Exploration Analysis
  * "Get to know" the dataset
  * Data cleaning
  * Think of ideas for feature engineering
  * Get a feel for the dataset but make this efficient

  * Includes qualitative and quantative analysis:
    * How many observations?
    * How many features?
    * Do the columns make sense?
    * Are the values on the right scale?


