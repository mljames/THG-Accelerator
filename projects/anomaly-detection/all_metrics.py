# A script containing all the metrics of a single site

metric_list=["site_speed","site_page_count","site_page_count_js","site_uniq","basket_adds_count","basket_adds_js","basket_visits","checkout_count","order_count_nobot","checkout_from_accountCreate","checkout_from_login","checkout_from_mybasket","product_adds_per_page_counts","basket_visits_per_product_adds","check_visists_per_basket_visits","checkout_visits_per_product_adds","orders_per_page_counts","orders_per_session_counts","orders_per_checkout","asia_count","asia_count_js","europe","europe_js","middle_east","middle_east_js","north_america","north_america_js","pacific","pacific_js","uk","uk_js","home","home_js","html","html_js","list","list_js","search","search_js","list_to_product","search_to_product","home_speed","html_speed","list_speed","search_speed","asia_speed","europe_speed","north_america_speed","pacific_speed","uk_speed","all_affiliate","awin","bing","cpc","deal_sites","facebook","google","mppp","site_blog","thgemail","thggpsad","thgpartner","thgppc","thgsocial","yahoo","zanox","basket_all_affiliate","basket_awin","basket_mppp","basket_thgemail","basket_thggpsad","basket_thgpartner","basket_thgppc","basket_thgsocial","basket_zanox","existing_customer","checkout_alipay","order_discount_nobot","new_"]

list_of_sites=["site1.","site2.","site3.","site4","site5"]

list_of_datasets=["data1","data2","data3","data4"]
