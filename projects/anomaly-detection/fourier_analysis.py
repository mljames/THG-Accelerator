from scipy.fftpack import fft, ifft
import numpy as np
import matplotlib.pyplot as plt
from anomaly_bot import strip_unix
import json

# INITAL ADMIN

def dataset_load(load_name):
    loaded_data = open(load_name,"r")
    load_data = json.load(loaded_data)[0]["datapoints"]
    loaded_data.close()
    return load_data

# FFT

def FFT(selected_data, name):

    # Perform FFT

    stripped_data = strip_unix(selected_data)
    Y = np.fft.fft(stripped_data)
    N = (len(Y)/2)+1

    # Sampling period is 1800s. Nyquists theorem says that we can therefore sample frequencies
    # at most half of the maximum frequency. The X axis is made up of N (length of half of the
    # FFT signal) equally spaced values up to the Nyquist frequency.

    dt = 1800
    fa = 1.0/dt

    print('dt=%.5fs (Sample Time)' % dt)
    print('fa=%.2fHz (Frequency)' % fa)

    X = np.linspace(0, fa/2, N, endpoint=True)

    # Finally we convert the x axis from frequency to period (in hours) for readability.

    Xp = 1.0/X
    Xph = Xp/(60.0*60.0)
    plt.figure(figsize=(15,6))
    plt.plot(Xph, 2.0*np.abs(Y[:int(N)])/int(N))
    plt.title("Fourier Transform to highlight periodicities in data ("+name+")")
    plt.xticks([8, 12, 24, 168, 336])
    plt.xlim(0, 336)
    plt.xlabel("Period of variation ($h$)")
    plt.ylabel("Amplitude (arbitary units)")
    plt.savefig("Anomaly Detection Trends ("+name+")",bbox_inches='tight', dpi=150, transparent=True)

if __name__=="__main__":_

    # A little bit of regex to extract name of file for graphs - we now simply change
    # load name to name of alternative file and the rest is done

    load_name = "data1_OrdersPerCheckout.json"
    selected_data = dataset_load(load_name)
    p = re.search('(?<=_)[^\.]+',load_name)
    name = p.group()

    FFT(selected_data, name)
