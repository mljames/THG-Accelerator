import pandas as pd
from fbprophet import Prophet
import matplotlib.pyplot as plt

file_name = open("data1_site1","r")
df = pd.read_json(file_name,lines=True)


number_of_nulls = []
for index,row in df.iterrows():
    count = 0
    for el in row[0]["datapoints"]:
        if el[0] == 0:
            count += 1
    number_of_nulls.append(count)
    print(str(index)+" FINISHED!!")

site1_speed = df.iloc[0][0]["datapoints"]
for el in site1_speed:
    el[1] = str(pd.to_datetime(el[1],unit='s'))

site1_speed_dates=[]
site1_speed_data=[]
for el in site1_speed:
    site1_speed_dates.append(el[1])
    site1_speed_data.append(el[0])

d = {'ds':site1_speed_dates,'y':site1_speed_data}

df = pd.DataFrame(data=d)


m = Prophet()
m.fit(df)
future = m.make_future_dataframe(periods=365)
future.tail()
forecast = m.predict(future)
fig1 = m.plot(forecast)

plt.show()
