import json
import re
from anomaly_bot import strip_unix, dataset_load
import matplotlib.pyplot as plt

def plot_all(selected_data, name):
    plt.plot(selected_data,'r')
    plt.xlabel("Time (total of 1 year)")
    plt.ylabel("Site1"+name+" (arbitary units)")
    plt.title("A time-series plot of an entire year for Site1 "+name)
    plt.savefig("All ("+name+")")
    plt.close()

if __name__=="__main__":

# A little bit of regex to extract name of file for graphs - we now simply change
# load name to name of alternative file and the rest is done

    load_name = "data1_Speed.json"
    selected_data = dataset_load(load_name)
    p = re.search('(?<=_)[^\.]+',load_name)
    name = p.group()

    stripped_data = strip_unix(selected_data)
    val = 0
    while val in stripped_data:
        stripped_data.remove(val)
    print(stripped_data)
    plot_all(stripped_data,name)
