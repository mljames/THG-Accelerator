import numpy as np
import json
import matplotlib.pyplot as plt
import re

# INITIAL ADMIN

# "GLOBAL VARIABLES" - given fourier analysis, it seems trends stronger by day,
# rather than by week

total_half_hours = 48

# Open File, for example in this case a prepared file for one metric of one site
# for the first dataset.

def dataset_load(load_name):
    loaded_data = open(load_name,"r")
    load_data = json.load(loaded_data)[0]["datapoints"]
    loaded_data.close()
    return load_data

# MODEL FOR SAME HALF AN HOUR EACH DAY

# Strip unix time off - starts at 10:30pm on a Tuesday

def strip_unix(raw_data):
    strip_unix = []
    for element in raw_data:
        strip_unix.append(element[0])
    return strip_unix

# Calculate the mean for the same half-an-hour from day-to-day for the entire year.

def calculate_mean(stripped_data,decision_boundary):
    mean_sample = []
    counter = 0
    while counter < total_half_hours:
        sample = []
        for i in range(0,len(stripped_data),total_half_hours):
            if i%total_half_hours==0 and stripped_data[i]!=0 and stripped_data[i]<decision_boundary:
                sample.append(stripped_data[i])
        mean_sample.append(np.mean(sample))
        stripped_data.pop(0)
        counter += 1
    return mean_sample

# Calculate the standard deviation of a particular half-an-hour.

def calculate_sd(stripped_data,mean,decision_boundary):
    sd_sample = []
    counter = 0
    while counter < total_half_hours:
        sample = []
        for i in range(0,len(stripped_data),total_half_hours):
            if i%total_half_hours==0 and stripped_data[i]!=0 and stripped_data[i]<decision_boundary:
                sample.append((stripped_data[i]-mean[counter])**2)
        sd_sample.append(np.sqrt(sum(sample)/len(sample)-1))
        stripped_data.pop(0)
        counter +=1
    return sd_sample

# Calculate the difference between the minimum and maximum within a half-an-hour
# period.

def calculate_max_min_diff(stripped_data):
    diff = []
    counter = 0
    while counter < total_half_hours:
        sample = []
        for i in range(len(stripped_data)):
            if i%total_half_hours==0 and stripped_data[i]!=0:
                sample.append(stripped_data[i])
        diff.append(max(sample)-min(sample))
        stripped_data.pop(0)
        counter+=1
    return diff

# Anomaly detection.

def anomaly_detector(stripped_data,mean,sd):
    anomalies = []
    datapoint = 0
    half_hour = 0
    for val in stripped_data:
        if val**2>(mean[half_hour]+2*sd[half_hour])**2 or val**2<(mean[half_hour]-2*sd[half_hour])**2 and val!=0:
            anomalies.append([val,datapoint])
        half_hour+=1
        half_hour=half_hour%total_half_hours
        datapoint+=1
    return anomalies

# PLOTS

def plot_average(average, name):
    plt.plot(average,'r')
    plt.xlabel("1 day in units of 30 minutes")
    plt.ylabel("Site1"+name+" (arbitary units)")
    plt.title("A time-series plot of the mean of the same half-an-hour period \n each day over the course of a year")
    plt.savefig("Mean ("+name+")")
    plt.close()

def plot_sd(sd, name):
    plt.plot(sd,'r')
    plt.xlabel("1 day in units of 30 minutes")
    plt.ylabel("Site1"+name+" (arbitary units)")
    plt.title("A time-series plot of the sd of the same half-an-hour period \n each day over the course of a year")
    plt.savefig("Std ("+name+")")
    plt.close()

def plot_diff(diff, name):
    plt.plot(diff,'r')
    plt.xlabel("1 day in units of 30 minutes")
    plt.ylabel("Site1"+name+" difference (arbirary units)")
    plt.title("A time-series plot of the max-min-difference of the same \n half-an-hour period each day over the course of a year")
    plt.savefig("Difference ("+name+")")
    plt.close()

def plot_anomaly(anomalies, name):
    for i in anomalies:
        plt.scatter(i[1],i[0],s=2)
    plt.xlabel("1 year in units of 30 minutes")
    plt.ylabel("Site1 anomalies of"+name+" (arbitary units)")
    plt.title("A time-series plot of the anomalies of "+name+" \n as they appeared within the last year")
    plt.savefig("Anomalies ("+name+")")
    plt.close()

# RUN

if __name__=="__main__":

    # A little bit of regex to extract name of file for graphs - we now simply change
    # load name to name of alternative file and the rest is done

    load_name = "data1_PageCount.json"
    decision_boundary = 15000
    selected_data = dataset_load(load_name)
    p = re.search('(?<=_)[^\.]+',load_name)
    name = p.group()

    stripped_data = strip_unix(selected_data)
    mean = calculate_mean(stripped_data,decision_boundary)
    sd = calculate_sd(stripped_data,mean,decision_boundary)
    diff = calculate_max_min_diff(stripped_data)
    print("The percentage of anomalies captured by the model is equal to "+str(len(anomalies)/len(stripped_data)*100)+" %")
    print(name)
    plot_average(mean,name)
    plot_sd(sd,name)
    plot_diff(diff,name)
    plot_anomaly(anomalies,name)
