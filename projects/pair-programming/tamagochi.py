import time

class tamagochi():
    def __init__(self,name,hunger = 50, tiredness = 50 ,happiness = 50):
        self.name = name
        self.hunger = hunger
        self.tiredness = tiredness
        self.happiness = happiness
        self.fullness = 100 - hunger
        self.life()

    def life(self):
        checker  = 0
        while True:
            print('Hunger: ',self.hunger,' Tiredness: ',self.tiredness,' Happiness: ',self.happiness,'Fullness:',self.fullness)
            self.happiness -= 2
            self.tiredness += 3
            self.hunger += 1
            self.fullness -= 2
            if self.kill_check():
                print(self.name, ' perished')
                break
            if self.hunger >= 75 and self.hunger%5==0:
                print("Hunger has dipped, its feeding time ahoihoi")
                user = input("Feed him or he might perish, say 'feed' to feed... ")
                if user == "feed":
                    self.feed()
            if self.tiredness >= 75 and self.tiredness%5==0:
                print("Tiredness has dipped, its sleeping time ahoihoi")
                user = input("Let him sleep or he might perish, say 'sleep' to sleep... ").lower()
                if user == "sleep":
                    self.sleep(50)
            if self.happiness <= 25 and self.happiness%5==0:
                print("Happiness has dipped, it's playtime ahoihoi")
                user = input("Make him happy or he might perish, say 'play' to play... ")
                if user == "play":
                    self.play()
            if self.fullness >= 75 and self.fullness%5==0:
                print("Fullness has peaked, it's pooing time ahoihoi")
                user = input("Let him poo or he might perish, say 'poo' to poo... ")
                if user == "poo":
                    self.poop()
            time.sleep(1)

    def kill_check(self):
        if self.hunger >= 100:
            return True
        elif self.tiredness >= 100:
            return True
        elif self.happiness <= 0:
            return True
        else:
            return False

    def feed(self):
        current_hunger = self.hunger - 50
        current_fullness = self.fullness + 50

        self.set_hunger(current_hunger)
        self.set_fullness(current_fullness)

    def play(self):
        current_happiness = self.happiness + 50
        current_tiredness = self.tiredness + 50

        self.set_happiness(current_happiness)
        self.set_tiredness(current_tiredness)

    def sleep(self,time):
        current_tiredness = self.tiredness - time
        self.set_tiredness(current_tiredness)

    def poop(self):
        current_fullness = self.fullness - 50
        self.fullness(current_fullness)

    def get_hunger(self):
        return self.get_hunger

    def get_tiredness(self):
        return self.tiredness

    def get_happiness(self):
        return self.happiness

    def get_fullness(self):
        return self.fullness

    def set_hunger(self, hunger):
        if hunger < 100 and hunger > 0:
            self.hunger = hunger

    def set_tiredness(self, tiredness):
        if tiredness < 100 and tiredness > 0:
            self.tiredness = tiredness

    def set_happiness(self, happiness):
        if happiness < 100 and happiness > 0:
            self.happiness = happiness

    def set_fullness(self, fullness):
        if fullness < 100 and fullness > 0:
            self.fullness = fullness

if __name__ == '__main__':

    Matthew = tamagochi('Matthew')
