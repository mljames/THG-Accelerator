import sqlite3

import click
from flask import current_app, g # g is a special object that is unique for each request - it is used to store data that might be accessed by multiple functions during the request
from flask.cli import with_appcontext

# current_app is a special object that points to the Flask application handling the request - we used an application factory so there is no application object when writing the rest of our code therefore current_app can be used

def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row # return rows that behave like dicts
    return g.db

def close_db(e=None):
    db = g.pop('db', None)
    if db is not None:
        db.close()

# open resource() opens a file relative to the flaskr package - this is useful as we won't necessarily know where that location is when deploying the application later
        
def init_db():
    db = get_db()
    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))
        
@click.command('init-db') # initiates a command line command called init-db that calls the init_db function and shows a success message to the user
@with_appcontext
def init_db_command():
    """Clear the existing data and create new table."""
    init_db()
    click.echo('Initialized the database')

def init_app(app):
    app.teardown_appcontext(close_db) # tells Flask to call that function when cleaning up after returning the response
    app.cli.add_command(init_db_command) # adds a new command that can be called with the flask command
