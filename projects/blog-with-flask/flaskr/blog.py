from flask import Blueprint, flash, g, redirect, render_template, request, url_for
from werkzeug.exceptions import abort
from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('blog', __name__) # does not have an url_prefix so the index view will be at /, the create view at /create and so on

@bp.route('/')
def index():
    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username, likes'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'   
        ).fetchall()
    comments = db.execute(
        'SELECT *'
        'FROM comment'
        ).fetchall()
    return render_template('blog/index.html', posts=posts,comments=comments)

@bp.route('/<int:id>/likes',methods=('POST',))
def likes(id):
    db = get_db()
    post = get_post(id)
    db.execute(
        'UPDATE post SET likes = (likes + 1)'
        'WHERE id = ?',
        (post['id'],)
        )
    db.commit()
    return redirect(url_for('blog.index'))


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO post (author_id, title, body, likes)'
                ' VALUES (?, ?, ?, ?)',
                (g.user['id'], title, body, 0)
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/create.html')

def get_post(id, check_author=False):
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post AS p JOIN user AS u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post

@bp.route('/<int:id>/comment',methods=('GET','POST'))
@login_required
def comment(id):
    post = get_post(id)

    if request.method == 'POST':
        comments = request.form['comment']
        error = None
    
        if not comments:
            error = 'Comment is required'
        
        if error is not None:
            flash(error)

        else:
            db = get_db()
            db.execute(
                'INSERT INTO comment(body,author_id,post_id)'
                'VALUES(?,?,?)',
                (comments,g.user['username'],post['id'])
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/comment.html',post=post)

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/update.html', post=post)

@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.index'))
