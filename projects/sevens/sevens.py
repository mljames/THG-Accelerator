# Game of 7s

import numpy as np
import matplotlib.pyplot as plt

class Sevens():

    def __init__(self, num_players, limit):
        self.num_players = num_players
        self.direction = 'anticlockwise'
        self.score = 1
        self.limit = limit
        self.current_player = None

    def select_first_player(self):
        self.current_player = np.random.randint(0,self.num_players)

    def select_random(self):
        random_number = np.random.choice(self.limit)
        return random_number if random_number != self.score else self.select_random()

    def switch_direction(self):
        return 'clockwise' if self.direction == 'anticlockwise' else 'anticlockwise'

    def next_player(self):
        if self.direction == 'clockwise':
            self.current_player = (self.current_player + 1)%self.num_players
        else:
            self. current_player = (self.current_player - 1)%self.num_players

    def has7(self):
        if "7" in str(self.score):
            self.direction = self.switch_direction()
            return True
    def mult7(self):
        if self.score%7==0:
            self.direction = self.switch_direction()
            return True
    def sum7(self):
        add = 0
        for s in str(self.score):
            add += int(s)
        if add == 7:
            self.direction = self.switch_direction()
            return True

    def one_play(self):
        plays = self.select_random()
        self.print_(plays)
        self.next_player()
        self.score += 1

    def play_game(self):
        array = np.zeros((self.limit))
        self.select_first_player()
        while self.score < self.limit:
            if self.has7():
                self.one_play()
            elif self.mult7():
                self.one_play()
            elif self.sum7():
                self.one_play()
            else:
                self.print_(self.score)
                self.next_player()
                array[self.score-1] = self.current_player
                self.score += 1
        return array

    def print_(self,plays):
        return print("Player", self.current_player, "says", str(plays))

if __name__ == "__main__":
    array = Sevens(23,23).play_game()
    plt.plot(array)
    plt.show()
