import unittest
from sevens import Sevens

class testSevens(unittest.TestCase):

    def setUp(self):
        self.game = Sevens(5,1)
    def test_mult7(self):
        self.game.score = 7
        self.assertEqual(self.game.mult7(), True)
    def test_sum7(self):
        self.game.score = 25
        self.assertEqual(self.game.sum7(), True)
    def test_has7(self):
        self.game.score = 27
        self.assertEqual(self.game.has7(), True)
    def test_random(self):
        self.game.score = 1
        self.assertNotEqual(self.game.select_random(),self.game.score)
    def test_direction(self):
        self.game.direction = 'anticlockwise'
        direction = self.game.switch_direction()
        self.assertNotEqual(direction,'anticlockwise')
    def test_select_first_player(self):
        initially = self.game.current_player
        self.game.select_first_player()
        self.assertNotEqual(initially, self.game.current_player)
    def test_next_player(self):
        self.game.current_player = 2
        initially = self.game.current_player
        self.game.direction = 'clockwise'
        self.game.next_player()
        self.assertEqual(initially+1, self.game.current_player)

if __name__ == "__main__":
    unittest.main()
