# Python document for solving the coloring problem

#!/usr/bin/python

def check_valid(graph):
    for node, neighs in graph.items():
        assert(node not in neighs) # check that no node links to itself
        for neigh in neighs:
            assert(neigh in graph and node in graph[neigh]) # check that if A linked to B B linked to A

class MapColor:

    def __init__(self, graph, colors):
        check_valid(graph)
        self.graph = graph
        nodes = list(self.graph.keys())
        self.node_colors  = {node: None for node in nodes}
        self.domains = {node: set(colors) for node in nodes} # where the set data type contains an unordered collection of unique and immutable objects
    def solve(self):
        uncolored_nodes = [n for n,c in self.node_colors.items() if c is None]
        if not uncolored_nodes:
            print(self.node_colors)
            return True
        node = uncolored_nodes[0]
        print('the nodes still left to color are:',uncolored_nodes)
        for color in self.domains[node]:
            if all(color != self.node_colors[n] for n in self.graph[node]):
                self.set_color(node, color)
                self.remove_from_domains(node, color)
                if self.solve():
                    return True
                self.set_color(node, None)
                self.add_to_domains(node, color)

        return False

    def set_color(self, key, color):
        self.node_colors[key] = color

    def remove_from_domains(self, key, color):
        for node in self.graph[key]:
            if color in self.domains[node]:
                self.domains[node].remove(color)
                if bool(self.domains[node])==False:
                	self.domains[node].add(color)
                	self.set_color(node,None)

    def add_to_domains(self, key, color):
        for node in self.graph[key]:
            self.domains[node].add(color)



WA  = 'western australia'
NT  = 'northwest territories'
SA  = 'southern australia'
Q   = 'queensland'
NSW = 'new south wales'
V   = 'victoria'
T   = 'tasmania'

colors    = {'r', 'g', 'b','y'}

australia = { T:   {V                },
              V:   {SA, NSW, T       },
              NSW: {Q, SA, V         },
              Q:   {NT, SA, NSW      },
              NT:  {WA, Q, SA        },
              WA:  {NT, SA           },
              SA:  {WA, NT, Q, NSW, V}
              
              }

#problem = MapColor(australia, colors)

AL = "Alabama"
AK = "Alaska"
AZ = "Arizona"
AR = "Arkansas"
CA = "California"
CO = "Colorado"
CT = "Connecticut"
DE = "Delaware"
FL = "Florida"
GA = "Georgia"
HI = "Hawaii"
ID = "Idaho"
IL = "Illinois"
IN = "Indiana"
IA = "Iowa"
KS = "Kansas"
KY = "Kentucky"
LA = "Louisiana"
ME = "Maine"
MD = "Maryland"
MA = "Massachusetts"
MI = "Michigan"
MN = "Minnesota"
MS = "Mississippi"
MO = "Missouri"
MT = "Montana"
NE = "Nebraska"
NV = "Nevada"
NH = "NewHampshire"
NJ = "NewJersey"
NM = "NewMexico"
NY = "NewYork"
NC = "NorthCarolina"
ND = "NorthDakota"
OH = "Ohio"
OK = "Oklahoma"
OR = "Oregon"
PA = "Pennsylvania"
RI = "RhodeIsland"
SC = "SouthCarolina"
SD = "SouthDakota"
TN = "Tennessee"
TX = "Texas"
UT = "Utah"
VT = "Vermont"
VA = "Virginia"
WA = "Washington"
WV = "WestVirginia"
WI = "Wisconsin"
WY = "Wyoming"

united_states_of_america = {
    FL: {AL, GA},
    AL: {GA, FL, TN, MS},
    GA: {SC, NC, TN, AL, FL},
    SC: {GA, NC},
    NC: {GA, TN, SC, VA},
    TN: {KY, MO, AR, MS, MO, AL, GA, NC},
    KY: {IN, IL, MO, TN, OH, WV, VA},
    IN: {MI, WI, IL, KY, OH},
    OH: {MI, IN, KY, WV},
    WV: {OH, VA, KY},
    VA: {WV, KY, NC},
    MI: {IL, WI, IN, OH},
    IL: {WI, IA, MO, KY, IN, MI},
    WI: {MN, IA, IL, MI, IN},
    MN: {ND, SD, IA, WI},
    ND: {MT, SD, MN},
    MT: {ID, WY, SD, ND},
    ID: {WA, MT, OR, WY, UT, NV},
    WA: {OR, ID},
    OR: {WA, ID, NV, CA},
    CA: {OR, NV, AZ},
    NV: {OR, ID, UT, AZ, CA},
	AZ: {CA, NV, UT, CO, NM},
    UT: {ID, NV, WY, CO, AZ, NM},
    WY: {MT, SD, NE, CO, UT, ID},
    CO: {WY, NE, KS, OK, NM, AZ, UT},
    NE: {SD, WY, CO, KS, MO, IA},
    SD: {ND, MT, WY, NE, MN, IA},
    IA: {MN, SD, NE, MO, WI, IL},
    MO: {IA, NE, KS, OK, AR, IL, KY, TN},
    OK: {KS, CO, NM, TX, AR, MO},
    KS: {NE, CO, OK, MO},
    AR: {MO, OK, TX, LA, TN, MS},
    MS: {TN, AR, LA, AL},
    LA: {AR, TX, MS},
    TX: {OK, NM, AR, LA},
    NM: {AZ, UT, CO, OK, TX},
    AK: {},
    CT: {},
    DE: {},
    HI: {},
    ME: {},
    MD: {},
    MA: {},
    NH: {},
    NJ: {},
    NY: {},
    PA: {},
    RI: {},
    VT: {},
}
usa = {FL : {AL,GA}}
values = [FL]
import numpy as np
def sort_dict(diction,current_diction,last_key,values):
	while bool(diction):
		for value in diction[last_key]:
			if value not in values:	
				usa.update({value : diction[value]})
				del diction[last_key]
				last_key = value
				values = np.append(values,value)
				return sort_dict(diction,usa,last_key,values)
			else:
				continue
		del diction[last_key]
		if len(list(diction))==0:
			return print(usa)
		key_ = np.random.choice(list(diction))
		usa.update({key_ : diction[key_]})
		last_key = key_
		values = np.append(values,key_)
		return sort_dict(diction,usa,last_key,values)
	return usa

sort_dict(united_states_of_america,usa,FL,values)

problem = MapColor(sort_dict(united_states_of_america,usa,FL,values),colors)

problem.solve()