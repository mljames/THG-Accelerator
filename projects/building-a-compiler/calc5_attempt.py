INTEGER, PLUS, MINUS, TIMES, DIVIDE, LBRA, RBRA, EOF = 'INTEGER', 'PLUS', 'MINUS', 'TIMES', 'DIVIDE', '(', ')', 'EOF'

class Token():
    def __init__(self, type, value):
        self.type = type
        self.value = value
class Lexer():
    def __init__(self, text):
        self.text = text
        self.pointer = 0
        self.current_char = self.text[self.pointer]
    def error(self):
        raise Exception('Invalid Character')
    def whitespace(self):
        while self.current_char.isspace() and self.current_char is not None:
            self.advance()
    def advance(self):
        self.pointer += 1
        if self.pointer > len(self.text) - 1:
            self.current_char = None
        else:
            self.current_char = self.text[self.pointer]
    def integer(self):
        result = ''
        while self.current_char is not None and self.current_char.isdigit():
            result += self.current_char
            self.advance()
        return int(result)
    def get_next_token(self):
        while self.current_char is not None:
            self.whitespace()
            if self.current_char.isdigit():
                return Token(INTEGER,self.integer())
            if self.current_char == '+':
                self.advance()
                return Token(PLUS, '+')
            if self.current_char == '-':
                self.advance()
                return Token(MINUS, '-')
            if self.current_char == '*':
                self.advance()
                return Token(TIMES, '*')
            if self.current_char == '/':
                self.advance()
                return Token(DIVIDE, '/')
            if self.current_char == '(':
                self.advance()
                return Token(LBRA, '(')
            if self.current_char == ')':
                self.advance()
                return Token(RBRA, ')')
            self.error()
        return Token(EOF, None)

class Interpreter():
    def __init__(self, lexer):
        self.lexer = lexer
        self.current_token = self.lexer.get_next_token()
    def error(self):
        raise Exception('Invalid syntax')
    def eat(self, token_type):
        if self.current_token.type == token_type:
            self.current_token = self.lexer.get_next_token()
        else:
            return self.error()
    def factor(self):
        token = self.current_token
        if token.type == INTEGER:
            self.eat(INTEGER)
            return token.value
        elif token.type == LBRA:
            self.eat(LBRA)
            result = self.expr()
            self.eat(RBRA)
            return result
    def term(self):
        result = self.factor()
        while self.current_token.type in (TIMES, DIVIDE):
            token = self.current_token
            if token.type == TIMES:
                self.eat(TIMES)
                result = result * self.factor()
            elif token.type == DIVIDE:
                self.eat(DIVIDE)
                result = result / self.factor()
            else:
                self.error()
        return result
    def expr(self):
        result = self.term()
        while self.current_token.type in (PLUS, MINUS):
            token = self.current_token
            if token.type == PLUS:
                self.eat(PLUS)
                result = result + self.factor()
            elif token.type == MINUS:
                self.eat(MINUS)
                result = result - self.factor()
            else:
                self.error()
        return result
def main():
    while True:
        try:
            text = input('calc>')
        except EOFError:
            break
        if not text:
            continue
        lexer = Lexer(text)
        # print(lexer)
        interpreter = Interpreter(lexer)
        result = interpreter.expr()
        print(result)
if __name__ == "__main__":
    main()
