# Building an Interpreter and Compiler

Derived from https://ruslanspivak.com/lsbasi-part1/

## Week 1

* What is an interpreter?
  * A translator that executes the source program without translating it into 'machine language' first.
* What is a compiler?
  * A translator that executes the source program by translating it into 'machine language' first.
* What's the difference between an interpreter and a compiler?
  * The goal of both an interpreter and a compiler is to translate a source program in some high-level language into some other form - an interpreter and compiler differ only in the steps they take to do this as covered above.
* What is a token?
  * A token is an object that has a type and a value. For example, for the string "3" the type of the token will be INTEGER and the corresponding value will be integer 3.
* What is the name of the process that breaks input apart into tokens?
  * Lexical analysis.
* What is the part of the interpreter that does lexical analysis called?
  * Lexical analyser.
* What are the other common names for that part of an interpreter or a compiler?
  * Scanner or tokeniser.

## Week 2

* What is a lexeme?
  * A lexeme is a sequence of characters that form a token, for example an example of a lexeme may be 342 which is an INTEGER token.
* What is the name of the process that finds the structure in the stream of tokens, or put differently, what is the name of the process that recognises a certain phrase in that stream of tokens?
  * Parsing.
* What is the name of the part of the interpreter (compiler) that does parsing?
  * Parser.

## Week 3

* What is a syntax diagram?
  * A syntax diagram is a graphical representation of a programming language's syntax rules. A syntax diagram visually shows you which statements are allowed in your programming language and which are not.
* What is a syntax analysis?
  * Parsing is also called a syntax analysis.
* What is a syntax analyser?
  * A parser is a syntax analyser.

## Week 4

* What is a context-free grammar?
  * A grammar specifies the syntax of a programming language in a concise manner. Unlike syntax diagrams, grammars are very compact. A grammar can serve as great documentation and is a good starting point even if you manually write your parser from scratch. One can often just convert the grammar to code by following a set of simple rules.
* How many rules / productions does the grammar have?
  * A grammar may look like: `expr : factor((MUL|DIV)factor)*` and `factor : INTEGER`. A grammar consists of a sequence of rules, also known as productions. There are two rules in a grammar. A rule consists of a non-terminal, called the head of the production, a colon, and a sequence of terminals and / or non-terminals called the body of the production.
* What is a terminal?
  * Tokens are called terminals.
* What is a non-terminal?
  * Variables like expr and factor are called non-terminals.
* What is the head of a rule?
  * The head of the rule, also called the left-hand side, is called the start symbol, and always contains a non-terminal, i.e. not a Token. For example a rule may start with `expr : `.
* What is the body of a rule?
  * The body is a sequence of terminals and / or non-terminals which provides the logic for the production.

## Week 5

* What does it mean for an operator to be left-associative?
  * If an operator is left-associative it means if an integer is sandwiched between two of these operators the left hand operator will be favoured.
* Are operators + and - left-associative? What about * and / ?
  * They are all left-associative.
* Does operator + have higher precedence than operator * ?
  * * and / have a higher precedence than + and - .
