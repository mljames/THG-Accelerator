# Accompanying .md file for assignment 1

1.
  * Entity diagram completed and shown in `Example_er_1.png`.
  * Primary keys identified were `Contact Email` and `id` for `Seller` and `Product` respectively.

2.
  * Entity diagram completed and shown in `Example_er_2.png`.
  * A key is an identifying attribute to an entity, that is to say it uniquely identifies an entity within its entity set.
  * A composite key is a key that requires more than one attribute to uniquely identify an entity.
  * Total participation refers to an entity within a relationship that **always** participates in the relationship.
  * A (single-valued) key constraint occurs when every entity participates in **exactly** one relationship.
  * Total participation is shown in an ER diagram with the following symbol joining the relevant relationship and entity -|-|- or -|-> depending on the cardinality.
  * A key constraint is identified by the symbol -|-|- joining the relevant relationship and entity, where in this case the cardinality is enforced to be 1.
  * "Each workshop must have an identified organiser among the conference participants" - `organiser` attribute to `attended by` relationship.
  * "Every participant must register for at least one workshop" - this was actually assumed already in the original scheme. The difference is in the -|- rather than -0- used joining an entity and a relationship, where -|- is a mandatory relationship (of some cardinality) and -0- is an optional relationship, which may take the value of 0 therefore.
3.
  * Entity diagram completed and shown in `Example_er_3.png`.
  * `Name of Field` and `Email Address` are the two primary keys.
  * We might want to include more information about the `Expert`, that is include more attributes, and the same for the `Field`.
  * A field may actually have more than one expert, if there were subfields to a particular field.
