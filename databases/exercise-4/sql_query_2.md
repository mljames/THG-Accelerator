## Creating tables in SQL and inserting values in to them

```sql  
-- Create table;

CREATE TABLE employee (
    FNAM VARCHAR(20),
    LNAME VARCHAR(20),
    NI INT PRIMARY KEY,
    BDATE DATE,
    ADDRESS VARCHAR(20),
    GENDER VARCHAR(1),
    SALARY INT,
    SUPERNI INT,
    DNO INT
);

CREATE TABLE department (
    DNAME VARCHAR(40),
    DNUM INT PRIMARY KEY,
    MGRNI INT,
    MGRSTARTDATE DATE,
    FOREIGN KEY(MGRNI) REFERENCES employee(NI) ON DELETE SET NULL
);

ALTER TABLE employee
ADD FOREIGN KEY(DNO)
REFERENCES department(DNUM)
ON DELETE SET NULL;

ALTER TABLE employee
ADD FOREIGN KEY(SUPERNI)
REFERENCES employee(NI)
ON DELETE SET NULL;

CREATE TABLE dept_locations (
    DNO INT,
    DLOCATION VARCHAR(40),
    PRIMARY KEY(DNO, DLOCATION),
    FOREIGN KEY(DNO) REFERENCES department(DNUM) ON DELETE CASCADE
);

CREATE TABLE project (
    PNAME VARCHAR(20),
    PNUM INT PRIMARY KEY,
    PLOCATION VARCHAR(40),
    DNO INT,
    FOREIGN KEY(DNO) REFERENCES department(DNUM) ON DELETE SET NULL
);

CREATE TABLE works_on (
    ENI INT,
    PNO INT,
    PRIMARY KEY(ENI,PNO),
    FOREIGN KEY(ENI) REFERENCES employee(NI) ON DELETE CASCADE
);

CREATE TABLE dependent (
  ENI INT,
  DEPENDENT_NAME VARCHAR(20),
  GENDER VARCHAR(1),
  BDATE DATE,
  RELATIONSHIP VARCHAR(10),
  FOREIGN KEY(ENI) REFERENCES employee(NI) ON DELETE SET NULL
);

INSERT INTO employee VALUES('Philip', 'Wilson', 1, '1970-09-23', 'Altrincham', 'M', 250000, NULL, NULL);
INSERT INTO department VALUES('Accelerator Programme', 1, 1,'2015-09-01')

UPDATE employee
SET DNO = 1
WHERE NI = 1;

INSERT INTO employee VALUES('Matthew', 'James', 3000, '1996-13-03', 'Castlefield', 'M', 30000, 1, 1);
```

One must be careful in the order of which tables are created and references are made, as well too the order of which rows are inserted in to these tables. The above SQL may start to convince you of this.

Creating a database and adding entries to it is quite cumbersome...
