# SQL Cheatsheet

* Herein lies a cheatsheet for SQL commands derived from a DataCamp course followed online.*

## What is a database?

A collection of related information that can be stored in different ways. A Database Management System (DBMS) is a special software program that helps users create and maintain a database:

* Makes it easy to manage large amounts of information
* Handles security
* Backups
* Importing/exporting data
* Interacts with software applications

The DBMS is required to create, read, update and delete information (CRUD). There are two types of databases:

* Relational Databases - organise data into one or more tables where each table has columns and rows and a unique key identifies each row
* Non-relational Databases - essentially any type of database that is not a relational database

A Relational Database Management System (RDBMS) help users create and maintain a relational database. Some examples include:

* mySQL
* Oracle
* postgreSQL
* mariaDB

They all use Structured Query Language (SQL) which is the standardized language for interacting with RDBMS. Queries are requests made to the database management system for specific information. As the database's structure become more and more complex it becomes very difficult to get the specific pieces of information we want!

## Tables and Keys

* A surrogate key is a key that has no mapping to something in the real world.
* A natural key on the other hand does have a mapping to a purpose in the real world, e.g. a social security number.
* A foreign key stores the primary key of a row in another database table. A foreign key is just a way to define relationships between two tables in a database. It is important to note that a table can have more than one foreign key.
* Also we can create a composite key where two primary keys are required to uniquely identify a single row.

## SQL Basics

SQL is a language used for interacting with RDBMS. We can use SQL to get the RDBMS to do things for us. SQL is actually a hybridg language; its basically 4 types of languages in one:

* Data Query Language
* Data Definition Language
* Data Control Language
* Data Manipulation Language

A query is a set of instructions given to the RDBMS that tell the RDBMS what information you want it to retrieve for you.

## Possible types

```sql
INT --- Whole Number
DECIMAL(M,N) --- Decimal Numbers
VARCHAR(1) --- String of text
BLOB  --- Binary Large Object
DATE --- 'YYYY-MM-DD'
TIMESTAMP --- 'YYYY-MM-DD HH:MM:SS'
```

## Creating tables

```sql
CREATE TABLE student (
    student_id INT AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    major VARCHAR(20) DEFAULT 'undecided',
    PRIMARY KEY(student_id)
);

DESCRIBE student;

DROP TABLE student;

ALTER TABLE student ADD gpa DECIMAL(3,2);
ALTER TABLE student DROP COLUMN gpa;

INSERT INTO student(name,major) VALUES('Jack','Biology')
INSERT INTO student(name,major) VALUES('Kate','Sociology')
INSERT INTO student(name, major) VALUES('Claire','Chemistry')
INSERT INTO student(name,major) VALUES('Jack','Biology')
INSERT INTO student(name,major) VALUES('Mike','Computer Science')

SELECT * FROM student;

UPDATE student
SET name = 'Tom', major = 'undecided'
WHERE student_id = 1;

DELETE FROM student
WHERE name='Tom' AND major='undecided'
```

If we wanted to create more complicated tables, including foreign keys and such like, here is an example:

```sql
CREATE TABLE employee (
  emp_id INT PRIMARY KEY,
  first_name VARCHAR(40),
  last_name VARCHAR(40),
  birth_day DATE,
  sex VARCHAR(1),
  salary INT,
  super_id INT,
  branch_id INT
);

CREATE TABLE branch (
  branch_id INT PRIMARY KEY,
  branch_name VARCHAR(40),
  mgr_id INT,
  mgr_start_date DATE,
  FOREIGN KEY(mgr_id) REFERENCES employee(emp_id) ON DELETE SET NULL
);

ALTER TABLE employee
ADD FOREIGN KEY(branch_id)
REFERENCES branch(branch_id)
ON DELETE SET NULL;

ALTER TABLE employee
ADD FOREIGN KEY(super_id)
REFERENCES employee(emp_id)
ON DELETE SET NULL;

CREATE TABLE client (
  client_id INT PRIMARY KEY,
  client_name VARCHAR(40),
  branch_id INT,
  FOREIGN KEY(branch_id) REFERENCES branch(branch_id) ON DELETE SET NULL
);

CREATE TABLE works_with (
  emp_id INT,
  client_id INT,
  total_sales INT,
  PRIMARY KEY(emp_id, client_id),
  FOREIGN KEY(emp_id) REFERENCES employee(emp_id) ON DELETE CASCADE,
  FOREIGN KEY(client_id) REFERENCES client(client_id) ON DELETE CASCADE
);

CREATE TABLE branch_supplier (
  branch_id INT,
  supplier_name VARCHAR(40),
  supply_type VARCHAR(40),
  PRIMARY KEY(branch_id, supplier_name),
  FOREIGN KEY(branch_id) REFERENCES branch(branch_id) ON DELETE CASCADE
);
```
We must be careful to add data in the correct order to these tables. For rows with foreign keys we must insert null values until the row corresponding to the foreign key on the initial table has been created. From there we must `UPDATE` that intial row.

## SQL Queries

Order of query execution:

1. FROM and JOINs
2. WHERE
3. GROUP BY
4. HAVING
5. SELECT
6. DISTINCT
7. ORDER BY

## Selecting Columns

SQL stands for Structured Query Language and is a language used for interacting with data stored in a relational database. In SQL keywords are not case-sensitive but its good practive to distinguish them from other parts of a query by putting them in uppercase. It's also good practive to include a semicolon at the end of a query. For example:

```sql
SELECT name
FROM people;

SELECT *
FROM people
LIMIT 10;

SELECT DISTINCT language
FROM films;

SELECT COUNT(*)
FROM people;

SELECT COUNT(DISTINCT birthdate)
FROM people;
```

A note on `COUNT`, `COUNT(*)` will count all null and non-null values in a column whereas `COUNT(column_name)` will count only non-null values.

## Filtering Rows

The `WHERE` keyword allows us to filter based on both text and numeric values in a table. The different comparison operators are:

* `=` equal
* `<>` not equal
* `<` less than
* `>` greater than
* `<=` less than or equal to
* `>=` greater than or equal to

For example:

```sql
SELECT title
FROM films
WHERE title = 'Metropolis';
```

One must use single quotes when using `WHERE` with text results.

Often we'll want to select data based on multiple conditions; we can build up `WHERE` queries by combining multiple conditions with the `AND` keyword. For example:

```sql
SELECT TITLE
FROM films
WHERE release_year > 1994
AND release_year < 2000;
```
If we wish to select rows based on multiple conditions where some but not all of the conditions need to be met then we can use the `OR` operator. When combining `AND` and `OR` we must enclose the individual clauses in parentheses. For example:

```sql
SELECT title
FROM films
WHERE (release_year = 1994 OR release_year = 1995)
AND (certification = 'PG' OR certification = 'R');
```

The `BETWEEEN` keyword provides a useful shorthand for for filtering values within a specified range and is shorter than using multiple `AND` statements. Please note however that `BETWEEN` is *inclusive*. Thus for exmaple:

```sql
SELECT title
FROM films
WHERE release_year
BETWEEN 1994 AND 2000;
```

`WHERE` can get unwieldly if we want to filter based on many conditions. The `IN` operatoper allows us to specify multiple values in a WHERE clause. Thus for example:

```sql
SELECT name
FROM kids
WHERE age IN (2, 4, 6, 8, 10);
```

In SQL, `NULL` represents a missing or unknown value. We can check for `NULL` values using the expression 'IS NULL'. The opposite is the `IS NOT NULL` operator, useful when wanting to filter out missing values:

```sql
SELECT name
FROM people
WHERE birthdate IS NOT NULL;
```

So far we've only been able to filter text data when we've specified an exact string. It is useful to be able to search for a pattern rather than a specific text string. In SQL, the `LIKE` operator can be used in a `WHERE` clause to search for a pattern in a column. This requires the use of *wildcards*, familiar from bash syntax covered in Operating System lectures. There are two wildcards one can use with the `LIKE` command:

* `%` will match zero, one or many characters in text
* `_` will match a *single* character

```sql
SELECT name
FROM companies
WHERE name LIKE 'DataC_mp';
```
We may also use the `NOT LIKE` operator to find records that *don't* match the pattern we specify.

## Aggregate Functions

If we want to perform some calculation in a database, SQL provides *aggregate functions* to help us out. For example, here are a few of those functions:

```sql
SELECT AVG(budget)
FROM films;

SELECT MAX(budget)
FROM films;

SELECT MIN(budget)
FROM films;

SELECT SUM(budget)
FROM films;
```

We can combine aggregate functoins with `WHERE` to gain further insights from data. For example one can do:

```sql
SELECT SUM(budget)
FROM films
WHERE release_year >= 2010;
```

We can also perform basic arithmetic with symbols like `+`, `-`, `*` and `/`. A note on this however, SQL assumes that if you divide an integer by an integer we want an integer back - be careful when dividing therefore. If we wish to keep the decimal place then we must ensure at least one of the divisors have a decimal point.

One thing to note when using functions such as `MAX`, the column name of the result was just the name of the function that was used, so in this case 'max'. If we wish to use `MAX` twice then we get two columns returned with name 'max' which isn't very useful. SQL allows us to use *aliasing*, which simply means to assign a temporary name to something. To alias we use the `AS` keyword. For example:

```sql
SELECT MAX(budget) AS max_budget, MAX(duration) AS max_duration
FROM films;
```

Aliasing is helpful for making results more readable.

## Sorting, Grouping and Joins

This section is solely about sorting and grouping results to gain further insight. In SQL the `ORDER BY` keyword is used to sort results in ascending or descending order according to the values of one or more of the columns. By default `ORDER BY` will sort in ascending order. If we wish to sort the results in descending order we can use the `DESC` keyword. Thus for example:

```sql
SELECT title
FROM films
ORDER BY release_year DESC;
```

The `ORDER BY` command naturally sorts text in to alphabetical order by default.

The `ORDER BY` command can also be used to sort on multiple columns. It will sort by the first column specified, then sort by the next, then the next, and so on. For example:

```sql
SELECT birthdate, name
FROM people
ORDER BY birthdate, name;
```

In this example the `ORDER BY` sorts on the birth dates first and then sorts on the names in alphabetical order - the order is importatant!

We'll often need to aggregate results, for example we might want to count the number of male and female employees in your company. We therefore need to group all of the males together and count them, and then group all of the females together and count them. In SQL, `GROUP BY` allows us to group a result by one or more columns, such as:

```sql
SELECT sex, COUNT(*)
FROM employees
GROUP BY sex;
```

This will return a result with headings 'sex' and 'count' and rows 'male' and 'female'. Commonly, `GROUP BY` is used with *aggregate functions* like COUNT() or MAX(). A point worth noting, SQL will return an error if we try to `SELECT` a field that is not in your `GROUP BY` clause without using it to calculate some kind of value about the entire group. For example:

```sql
SELECT sex, COUNT(*)
FROM employees
GROUP BY sex
ORDER BY count DESC;
```

A point worth noting is that `ORDER BY` must alwats be put at the end of a query.

In SQL *aggregate* functions can not be used in `WHERE` clauses. The following query is therefore invalid:

```sql
SELECT release_year
FROM films
GROUP BY release_year
WHERE COUNT(title) > 10;
```

If we therefore want to filter based on the result of an aggregate function we need another way. The `HAVING` clause is used for this:

```sql
SELECT release_year
FROM films
GROUP BY release_year
HAVING COUNT(title) > 10;
```

This only shows years in which there were more than 10 films released.

This is all well and good, but in the real world we'd like to query multiple tables. For example if we want to see the IMDB score for a particular movie. In this case we'd want to get the ID of the movie from the 'films' table and then use it to get IMDB information from the 'reviews' table. In SQL this concept is known as a **join**, where a basic *join* is shown below:

```sql
SELECT title, imdb_score
FROM films
JOIN reviews
ON films.id = reviews.film_id
WHERE title = 'To Kill a Mockingbird';
```

The types of join include:

* Cross joins - return all combinations of rows from each table (cartesian product)
* Natural join - return rows where the join condition is met or true on all common columns - however natural joins are not used in standard SQL.
  * This will return only one copy of the column which is joined upon
* Inner joins - return rows where the join condition is met or true
  * This will return both copies of the column which is joined upon
* Outer joins - returns results of an inner join whilst also returning rows from the table that don't match the condition
  * Left outer join - return inner join and anything from the left table
  * Right outer join - return inner join and anything from the right table
  * Full outer join - return everything

The basic syntax for an `INNER JOIN`, here including all columns in **both** tables:

```sql
SELECT cities.name AS city, countries.name AS country, countries.region AS region
FROM cities
INNER JOIN countries
ON cities.country_code = countries.code;
```

If a column heading appears in both tables then we need the dot notation as shown in the example above.

The ability to combine multiple joins in a single query is a powerful feature of SQL. As an example:

```sql
SELECT c.code, name, region, e.year, fertility_rate, unemployment_rate
FROM countries AS c
INNER JOIN populations AS p
ON c.code = p.country_code
INNER JOIN economies AS e
ON c.code = e.code AND e.year = p.year
```

We may also use a `USING` command word rather than the `ON` command to join table as a shortcut, for example:

```sql
SELECT *
FROM countries
INNER JOIN economies
USING(code)
```

This is good since it won't throw an error if the two column names have different types.

We can also join a table to itself - this is called a self-join. This may look like:

```sql
SELECT p1.country_code,
       p1.size AS size2010,
       p2.size AS size2015,
       ((p2.size - p1.size) / p1.size * 100.0) AS growth_perc
FROM populations AS p1
INNER JOIN populations AS p2
ON p1.country_code = p2.country_code
AND p1.size = p2.size - 5;
```

Often it's useful to look at a numerical field not as raw data, but instead as being in different categories or groups. We can use `CASE` with `WHEN`, `THEN`, `ELSE` and `END` to define a new grouping field. For example:

```sql
SELECT name, continent, code, surfae_area,
    CASE WHEN surface_area > 2000000 THEN 'large'
    WHEN surface_area > 350000 THEN 'medium'
    ELSE 'small' END
    AS geosize_group
INTO countries_plus
FROM countries;
```

The `INTO` command creates another table to which it pumps the output of a query.

There are also 'RIGHT JOIN' and `LEFT JOIN` commands:

```sql
SELECT employee.emp_id, employee.first_name, branch.branch_name
FROM employee
LEFT JOIN branch
ON employee.emp_id = branch.mgr_id;

SELECT employee.emp_id, employee.first_name, branch.branch_name
FROM employee
RIGHT JOIN branch
ON employee.emp_id = branch.mgr_id;
```

## Set Operations

We may find the intersection of two searches using `UNION`, `INTERSECTS` and `EXCEPTS`, for example:

```sql
-- Find a list of all clients & branch suppliers' names

SELECT client_name, client.branch_id
FROM client
UNION [DISTINCT | ALL]
SELECT supplier_name, branch_supplier.branch_id
FROM branch_supplier;
```

The order and the number of columns must be the same and the data types of the corresponding columns must be compatible. There are some nuances with set operations:

* Column names
* ORDER BY clause only in the final query in the set statement
* GROUP BY and HAVING only in individual queries

UNION and EXCEPT have the same precedence order. INTERSECT takes higher precedence than the other set operations.

UNIONS combine rows, JOINS combine columns.

** mysql does not support INTERSECT and EXCEPT **

## Nested Queries

We can nest queries within queries following the convention:

```sql
-- Find names of all employees who have sold over 30,000 to a single client;

SELECT employee.first_name, employee.last_name
FROM employee
WHERE employee.emp_id IN(
    SELECT works_with.emp_id
    FROM works_with
    WHERE works_with.total_sales > 30000
);

-- Find all clients who are handled by the branch that Michael Scott manages.
-- Assume we know Michael's ID;

SELECT client.client_name
FROM client
WHERE client.branch_id = (
    SELECT branch.branch_id
    FROM branch
    WHERE branch.mgr_id = 102
    LIMIT 1
);
```

** You cannot update a table and select it with subqueries **

* We can write subqueries in the `WHERE` clause to check for existence, membership or comparison.
* We can also write subqueries in the `FROM` clause.


## On Delete

Why use `ON DELETE SET NULL` versus `ON DELETE CASCADE`? If your foreign key makes up a primary key or is a component of a primary key then it always has to be `ON DELETE CASCADE`

```sql
DELETE FROM employee
WHERE emp_id = 102;

DELETE FROM branch
WHERE branch_id = 2;
```

## Triggers

Triggers are useful when you want something to happen when another thing happens. We must set triggers in the terminal window. The following is an example of a trigger from the tutorial:

```sql
DELIMITER $$
CREATE
    TRIGGER my_trigger_ BEFORE INSERT
    ON employee
    FOR EACH ROW BEGIN
        IF NEW.sex = 'M' THEN
            INSERT INTO trigger_test VALUES("added male employe");
        ELSEIF NEW.sex = 'F' THEN
            INSERT INTO trigger_test VALUES("added female");
        ELSE
            INSERT INTO trigger_test VALUES("added other employee");
        END IF;
    END $$
DELIMITER ;

INSERT INTO employee
VALUES(109, 'Oscar', 'Martinez', '1968-02-19', 'M', 69000, 106, 3);

SELECT * FROM trigger_test_;
```

## ER Diagrams Intro

Jargon related to ER diagram construction:

* Entity - An object we want to model & store information about, for example a Student.

* Attributes - Specific pieces of information about an entity

* Primary Key - An attribute(s) that uniquely identify an entry in the database table

* Composite Attributes - An attribute that can be broken into sub-attributes

* Multi-values Attribute - An attribute that can have more than one value

* Derived Attribute - An attribute that can be derived from the other attributes

* Mutiple Entities - You can define more than one entity in the diagram

* Relationships - Defines a relationship between two entities

* Total Participation - All members must participate in the relationship

* Relationship Attribute - An attribute about the relationship

* Relationship Cardinality - **The number of instances of an entity from a relation that can be associated with the relation**

* Weak Entity - An entity that cannot be uniquely identified by its attributes alone

* Identifying relationship - A relationship that serves to uniquely identfy the weak entity

We can produce an ER diagram from spoken relationships. We can then convert an ER diagram to produce a database schema. The process of doing this may be summarised:

1) Mapping of Regular Entity Types - For each regular entity type create a relation table that includes all the simple attributes of that entity

2) Mapping of Weak Entity Types - For each weak entity type create a relation (table) that includes all simples attributes of the weak entity. The primary key of the new relation should be the partial key of the weak entity plus the primary key of its owner.

3) Mapping of Binary 1:1 Relationship Types - Include one side of the relationship as a foreign key in the other and **favour total participation** otherwise we will end up with null values in the table.

4) Mapping of Binary 1:N Relationship Types - Include the 1 side's primary key as a foreign key on the N side relation (table).

5) Mapping of Binary M:N Relationship Types - Create a new relation (table) who's primary key is a combination of both entities' primary key's. Also include any relationship attributes.

6) Mapping of Multivalued Attributes - Create a new relation (table) with that attribute as a foreign key.

## Transactions

A transaction is a unit of program execution that accesses and possibily updates various data items - it is a collection of SQL queries which forms a logical one task. For a transaction to be completed successfully all SQL queries have to run successfully.The DB transaction executes either all or none. This is required to protect data and keep it consistent when multiple users access the database at the same time.

For example: transaction to transfer £50.00 from account A to account B:

1. read(A)
2. A : A - 50
3. write(A)
4. read(B)
5. B := B + 50
6. write(B)

Two main issues to deal with:

1. Failures of various kinds
2. Concurrent execution

There is an *atomicity requirement*,a *consitency requirement*, an *isolation requirment* and a *durability requirement*.

Based on these requirements we have transaction states:

* Active - intial state; the transactions stays in this state while it is executing
* Partially committed - after the final statement has been executed
* Failed
* Aborted
* Committed - officially approved

General mysql transaction statements include:

* `START TRANSACTION`
* `COMMIT`
* `ROLLBACK`

Multiple transactions are allowed to run concurrently in the system, the advantages are:

* Increased processor and disk utilisation

A schedule is a sequence of instructions that specify the chonological order in which instrucitions of concurrent transactions are executed.

* Why is bad to read different information between two reads?
* How are transactions dealt with in practice?
* Can you explain benefits of concurrency? Why can't we just queue?
