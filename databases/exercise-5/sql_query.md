## Intermediate SQL Queries

### Exercise 1

```SQL
SELECT DISTINCT s.ID
FROM STUDENT AS s, TAKES as ta
WHERE s.ID = ta.ID AND ta.COURSE_ID = (
  SELECT t.COURSE_ID
  FROM INSTRUCTOR AS i, TEACHES AS t
  WHERE i.name = 'Einstein' AND i.ID = t.ID
);
```

`Answer : ID = 44553`

```SQL
SELECT NAME
FROM INSTRUCTOR
WHERE SALARY = (
  SELECT MAX(SALARY)
  FROM INSTRUCTOR
);
```

`Answer : NAME = Einstein`

```SQL
SELECT DISTINCT COUNT(t.COURSE_ID) AS ENROLLMENT, c.TITLE AS COURSE
FROM STUDENT AS s, TAKES AS t, COURSE AS c
WHERE s.ID = t.ID AND t.SEMESTER = "FALL" AND t.YEAR = 2009 AND c.COURSE_ID = t.COURSE_ID
GROUP BY c.TITLE;
```

`Answer : Intro. to Computer Science - 6; Database System Concepts - 2; Physical Principles - 1;`

```SQL
SELECT DISTINCT COUNT(t.COURSE_ID) AS MAX_ENROLLMENT, c.TITLE AS COURSE
FROM STUDENT AS s, TAKES AS t, COURSE AS c
WHERE s.ID = t.ID AND t.SEMESTER = "FALL" AND t.YEAR = 2009 AND c.COURSE_ID = t.COURSE_ID
GROUP BY c.TITLE
ORDER BY MAX_ENROLLMENT DESC
LIMIT 1;
```

`Answer : Intro. to Computer Science - 6;`

The last query is as above.

### Exercise 2

```SQL
SELECT NAME, TOT_CRED AS TOTAL_GRADE_POINTS
FROM STUDENT
WHERE s.ID = 12345;
```

`Answer : Shankar - 32`

*In the below query, the function grade_points converts a grade to score.*

`Answer : Zhang - 102; Shankar - 32; Brandt - 80; Chavez - 110; Peltier - 56; Levy - 46; Williams - 54; Sanchez - 38; Snow - 0;`

```SQL
SELECT s.ID AS ID, s.NAME AS NAME, SUM(CREDITS*grade_points(t.GRADE))/TOT_CRED AS GPA
FROM COURSE AS c, TAKES AS t, STUDENT AS s
WHERE t.COURSE_ID = c.COURSE_ID AND s.ID = t.ID
GROUP BY s.ID;
```

### Exercise 3

```SQL
SELECT i.ID, i.NAME, COUNT(s.COURSE_ID) AS Number_of_Courses
FROM INSTRUCTOR AS i, TEACHES AS t, SECTION AS s
WHERE i.ID = t.ID AND t.COURSE_ID = s.COURSE_ID
GROUP BY i.ID;
```
`Answer : 76766 - Crick - 2; 10101 - Srinivasan - 4; 45565 - Katz - 4; 83821 - Brandt - 6; 98345 - Kim - 1; 12121 - Wu - 1; 32343 - El Said - 1; 15151 - Mozart - 1; 22222 - Einstein - 1;`

```SQL
SELECT i.DEPT_NAME AS Department_Name, COUNT(i.ID) AS Number_of_Instructors
FROM INSTRUCTOR AS i
WHERE
GROUP BY i.DEPT_NAME;
```

`Answer : Biology - 1; Comp. Sci. - 3; Elec. Eng - 1; Finance - 2; History - 2; Music - 1; Physics - 2`

*This query will emulate a join*

```SQL
SELECT *
FROM STUDENT AS s, TAKES AS t
WHERE t.ID = s.ID;
```
