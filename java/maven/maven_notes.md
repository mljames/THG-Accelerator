## Notes from Maven Tutorial

```bash
mkdir myapp
cd myapp/
mvn archetype:generate
```

`Choose a number` - an archetype is a model which defines how we want our project to be structured. Each number corresponds to a different application which we might want to make, if we choose a number maven will create a blank application with all of the dependencies and all of the jars.

`Choose a version` - the versions of the archetype itself.

`Define value for property groupID: - org.jamesm.thg`
`Define value for property artifactId: - MavenTestApp`
`Define value for property version: - 1.0-SNAPSHOT`
`Define value for property package: - org.jamesm.thg`
`Confirm properties configuration: - Y`

Maven creates a directory called MavenTestApp with a pom.xml file and a src folder. The src folder contains a main and a test folder. Maven has created our file structure, all the required package directories as well as the pom.xml file.

Now lets try to compile the code. Go to the layer with pom.xml and type the command:

```bash
mvn compile - this compiles all of the classes in the src folder
mvn package - this has created the jar in the target directory - it also runs jUnit test cases
java -cp target/MavenTestApp-1.0-SNAPSHOT.jar org.jamesm.thg.App - this runs the jar file
```

Maven gets all of its knowledge from the Maven Repository which contains Archetype information and Dependency information. We run the archetype command and Maven returns the Directory Structure, as well as a pom.xml. A pom.xml has information about dependencies. We then did a compile and package. When we compiled and packaged Maven realised that pom.xml had this jUnit dependency and goes to the dependency information and checks what are the other dependent jars which it needs to get. The package command builds a jar file.

Let's have a closer look at what Maven has done for us:

* Project template
  * Folder Structure
  * pom.xml
    * Archetype -controls the layout of the files, dependencies etc.
    * Group ID
    * Artifact ID
    * Version
    * Package
* Build
  * Build lifecycle
  * Consists of phases
    * Validate - checks if everything is in order
    * Compile - takes all .java file and takes them to .class files
    * Test - run the test cases
    * Package - generates the artifact
    * Install - puts artifact into a local Maven repository
    * Deploy - publishing an artifact to a remote repository
  * Default behaviour of phases
  * Specify the build phase you need - previous phases automatically run e.g. if you specify a package it automatically runs compile and test
  
`mvn clean` removes the target folder.
