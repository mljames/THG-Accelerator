## Spring

#### What is the difference between frameworks and libraries?

There are a lot of similiarities and commonalities with a lot of the standard enterprise business applications that we build. Let's see...

* Authentication?
* Logger?
* Database connectivity?
* Web application?

Perhaps we can get these from an open source project...

Libraries are used to solve common problems and tasks so that developers can write code for the main problem that they're actually trying to solve. Libraries are:

* Reusable
* Well defined API
* Doesn't care how and when you use it

We have "toolboxes" to help with certain tasks, these can be thought of as libraries. However, if we think about many carpenters using the same tools to make the same thing, then we can provide a framework for the carpernters to help them build their tables. There is some customisation to how the table will look, but the general output is established by the framework.

#### Basics of Spring Framework

In Java world we have frameworks for building web applications, e.g.

* Listens to web requests
* Handles sessions
* Calls your code to handle requests

The difference is you call the library but the framework calls you. A framework makes things simpler but by its very nature is much less flexible. So, what to use when?

A Spring framework provides patterns and structure for an application. So what does the Spring framework provide?

* APPLICATION CONTEXT AND DEPENDENCY INJECTION: Handling interrelated dependencies among objects is a common problem - and Spring framework tries to help. When we add the Spring framework it acts as a container for our object instances and wraps our application in a wrapper called the Spring Application Context, which Spring manages.

* DATA ACCESS
  * Connectivity
  * Querying
  * Transaction management
  * and more...

* Sring MVC
  * Dynamic web page applications
  * Rest APIs

Spring has now become an ecosystem.

## Spring Boot Quick Start

#### What is Spring Boot?

* The Spring framework helps us write Enterprise Java Applications.
* Boot is Bootstrap - Spring Boot helps us Bootstrap a Spring application from scratch.

`"Spring Boot makes it easy too create stand-along, production-grade Spring based Applications that you can 'just run'."`

#### What is Spring?

* Spring provides an Application framework - it lets us build Enterprise Java Applications.
* Spring has a programming and configuration model.
* Spring lets us build classes which have annotations on them which denote what they are, for example a business service we write the code that is specific to our business service and then annotate that class with someothing like "@service", and Spring will apply a whole lot of things to that class and looks after teh lifecycle of that class.
* Spring provides infrastructure support.

However, there are certain problems with Spring.

* Spring is a huge network.
* Multiple setup steps.
* Multiple configuration steps.
* Multiple build and deploy steps.

Can we abstract these steps to deal with these problems?? This is what Spring Boot does!

* Spring Boot is opinionated - it makes certain certain decisions for us, can change things if required etc.
* Convention over configuration.
* Stand alone.
* Production ready.

#### Dependencies

We deal with dependencies using Maven. A maven project contains a pom.xml file. Maven is a build and dependency management tool. Maven also provides us with archetypes which gives us a simple project to start from.

`<dependencies>` tell us what jars to download, `<parent>` tells us what versions of these jars to download.

#### Starting a Spring application

Create a Maven project and add the following to the pom.xml.

```xml
   <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.4.2.RELEASE</version>
    </parent>
```

Our project is a child of this parent project, i.e. a spring-boot-starter-parent. This contains an opionated set of maven configurations, which are inherited.

Now, we must declare the dependencies.

```xml
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>

    <properties>
        <java.version>1.8</java.version>
    </properties>
```

We know if we're creating a web project, we know which jars you're going to need - therefore we can use this meta-dependency which manages all of the jars that we need. We also need to specify that we require Java 8:

```xml
    <properties>
        <java.version>1.8</java.version>
    </properties>
```

We then create a new class (in the correct package) called CourseApiApp, and the following code:

```java
@SpringBootApplication
public class CourseApiApp {
       public static void main(String[] args) {
              SpringApplication.run(CourseApiApp.class, args);
	      }
	}
```

This is our SpringBoot application!

* Sets up default configuration
* Starts Spring application context
* Performs class path scan - we mark our classes telling Spring how to treat a particular class; therefore Spring on starting up looks for special annotations, and if one does, Spring will treat that class accordingly.
* Starts Tomcat server 

Spring is a container for all of our code that runs on our application server. A container is an application context. The Spring Boot starter essentially creates this application context.

A controller is a Java class which has some annotations within it. An annotation lets Spring know what url we are mapping something to and what should happen when a request comes to that url.

#### Spring MVC

Spring MVC lets us build server side code, which maps to urls and provides responses - a response may be a REST response, e.g. JSON, or it could be a full html page etc. If we build a RESTApi we require the response to be a simple JSON / string. A REST controller within Spring Boot can be created by simply using:

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

```java
@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String sayHi() {
        return "Hi";
    }
}
```

sayHi() is executed when `/hello` url is hit. It's really simple.

The @RequestMapping maps only to the GET method by default. To map to other HTTP methods, you'll have to specify it in the annotation.

@RestController automatically produces JSON objects on the return page. Spring MVC does this conversion for us.

#### What's happening here?

1) pom.xml

In the pom.xml we have <parent></parent> and <dependencies></dependencies>. The <parent> is pulling the configuration and tells Maven what jars to pull when we do have a dependency. The dependency section tells Maven what jars to download and the parent section tells Maven what versions of these jars to download.

2) Embedded Tomcat Server

* Convenience
* Servlet container configuration is now part of the application configuration
* Standalone application
* Useful for microservices architecture

#### How Spring MVC works?

Spring MVC provides the VIEW tier, it allows us to map requests to responses by building controllers. Controllers are used for the logic of deciding what to do when a request comes in. These controllers are simple Java classes which map a URI and a HTTP Method, e.g. PUT, GET, POST etc.  to some functionality. If there is a match i.e. URI and HTTP Method, then it executes the relevant method, converts it to a proper response and sends it back.

What is a proper response? E.g as earlier, Spring MVC converted the response to a JSON for us.

#### REST RPI

The first thing to think about is what are the Resources of our REST API. The resources are the things in our domain - they're normally nouns. Once we have identified resources then we must think about how our users can access the resources using HTTP methods.

For our course API we have `topic, course and lesson` - a topic can have multiple courses and a course can consist of multiple lessons. For topics:

* GET --- /topics --- Gets all topics
* GET --- /topics/id --- Gets the topic
* POST --- /topics Create --- new topic
* PUT --- /topics/id --- Updates the topic
* DELETE --- /topics/id --- Deletes the topic

A business service in Spring starts with a simple Java class. In Spring business services are typically `singletons`. We can use the annotation `@Service` marks the class as a Spring business service. `@Autowired` is a Spring annotation which marks dependency injection for classes declared.

For the methods proposed above, the following code describes how one might implement them.

```java
    @RequestMapping("/topics")
    public List<Topic> getAllTopics() {
        return topicService.getAllTopics();
    }

    @RequestMapping("/topics/{id}")
    public Topic getTopic(@PathVariable String id) {
            return topicService.getTopic(id);
    }

    @RequestMapping(method=RequestMethod.POST,value="/topics")
        public void addTopic(@RequestBody Topic topic) {
                topicService.addTopic(topic);
    }

    @RequestMapping(method=RequestMethod.PUT,value="/topics/{id}")
    public void updateTopic(@RequestBody Topic topic, @PathVariable String id) {
            topicService.updateTopic(id, topic);
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/topics/{id}")
    public void deleteTopic(@PathVariable String id) {
            topicService.deleteTopic(id);
    }
```

Where `addTopic`, `updateTopic` and `deleteTopic` apply business logic, in a separate class called `TopicService`, where above live in `TopicController`.

```java
@Service
public class TopicService {
        private List<Topic> topics = new ArrayList<>(Arrays.asList(new Topic("spring","Spring Framework","Spring Framework Description"),
                new Topic("java","Core Java","Core Java Description"),
                new Topic("javascript","JavaScript","JavaScript Description")
                ));

        public List<Topic> getAllTopics() {
                return topics;
        }
        public Topic getTopic(String id) {
                return topics.stream().filter(t->t.getId().equals(id)).findFirst().get();
        }

        public void addTopic(Topic topic) {
                topics.add(topic);
        }

        public void updateTopic(String id, Topic topic) {
                for (int i=0; i<topics.size(); i++) {
                        Topic t = topics.get(i);
                        if (t.getId().equals(id)) {
                                topics.set(i,topic);
                                return;
                        }
                }
        }

        public void deleteTopic(String id) {
                topics.removeIf(t->t.getId().equals(id));
        }
}
```

#### Booting Spring Boot

* Starting a Spring Boot App
  * Spring Initializr --- `https://start.spring.io`
  * Spring Boot CLI (command line interface) --- `https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started-installing-spring-boot.html`
  * STS IDE
* Configuration

The Spring Boot CLI is a command line tool that can be used if we want to quickly prototype with Spring - it allows us to run Groovy scripts, which is useful for quick prototyping. A Spring groovy script is a script we can use to Bootstrap a complete Spring Boot application. You are unlikely to use this in practice. A simple groovy script:

```groovy
@RestController                                                                 
class AppCtrl {                                                                 
                                                                                
      @RequestMapping("/")                                                      
      String app() {                                                            
      "Hello world"                                                             
      }                                                                         
}
```

And run with `./spring run app.groovy`.

However, sometimes there is a need for us to configure Spring Boot. The way to customise Spring Boot applications is by using a properties file, e.g. application.properties, which we may find in the resources folder. This actuallly customises Spring itself. The .properties file has a load of key-value pairs that affects how Spring behaves. With SpringBoot there are special keys that we can use within this properties file to affect how SpringBoot works.

How do we know what to add to application.properties? --- `https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html.

#### Spring Data JPA : The Data Tier

Now we're going to be working with data sources. We're going to have our Spring Boot application connect to a database and perform CRUD operations. JPA stands for Java Persistence API. It is a specification that lets us do Object-Relational Mapping. A SQL database is a relational database, so when we need to connect from our Java application to a relational database we need to use JDBC, run SQL queries etc, get our results and convert to object instances. ORM lets us map entity classes into SQL tables so that when we connect to the database we provide metadata on our entity classes so that we don't have to do the query and then the mapping ourselves, but so that the framework does it for us. JPA is a way for us to use ORM.

Spring Data JPA makes working with these tools even easier.

We can make a Spring Boot application that allows us to talk to a database (internal) by simply creating a project within IntelliJ using Initializr and selecting `JPA` and `Apache Derby`.

N.B for SpringBoot code structure see https://www.tutorialspoint.com/spring_boot/spring_boot_code_structure.htm.

#### Adding the database

We need to save Topic instances - we need to map the Topic objects to a relational database table. How do we tell JPA that? **Use annoatations**.

1) @Entity
2) In a relational database we need a primary key - @Id

With this configuration we can convert a Topic instance to a row in the database table and a row in the database table to a Topic instance.

We now need to use TopicService.java to connect to the database and run those commands. All we need to do is create an interface TopicRepository and SpringBoot data JPA will be providing the class.

**should never use this in production environment**

#### Deployment and Monitoring

How to get our application ready for deployment.

`mvn clean install`
`java -jar target/course-api-data-0.0.1-SNAPSHOT.jar`

Spring Tutorial --- https://www.tutorialspoint.com/spring_boot/index.htm