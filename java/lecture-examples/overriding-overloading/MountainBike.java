public class MountainBike extends Bike {

    private int extendedWarranty;
    public MountainBike(int serial, String warranty, int extendedWarr)
    {
        super(serial,warranty);
        this.extendedWarranty=extendedWarr;
    }

    @Override
    public void Description() {
        System.out.println("10 years of warranty");
    }

    public void Description(int myValue) {
        System.out.println(myValue+" years of warranty");
    }

    public int getExtendedWarranty() {
        return extendedWarranty;
    }

    public void setExtendedWarranty(int extendedWarranty) {
        this.extendedWarranty = extendedWarranty;
    }
}
