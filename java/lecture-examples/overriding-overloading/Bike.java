public class Bike {
    private int bikeSerial;
    private String Warranty;

    public Bike(int bikeNumber, String myWarranty)
    {
        this.bikeSerial=bikeNumber;
        this.Warranty=myWarranty;
    }

    public void Description() {
        System.out.println("5 years of warranty");
    }

    public int getBikeSerial() {
        return bikeSerial;
    }

    public void setBikeSerial(int bikeSerial) {
        this.bikeSerial = bikeSerial;
    }

    public String getWarranty() {
        return Warranty;
    }

    public void setWarranty(String warranty) {
        Warranty = warranty;
    }
}
