import java.util.ArrayList;
import java.util.List;

public class Main {

    public static <T> void main(String[] args) {

        GenericSortingClass GenericSorter = new GenericSortingClass();

        List<String> fruitArray = new ArrayList<>();
        List<Integer> salaryArray = new ArrayList<>();
        List<Double> decimalArray = new ArrayList<>();

        fruitArray.add("Apple");
        fruitArray.add("Banana");
        fruitArray.add("Pineapple");
        fruitArray.add("Pear");
        fruitArray.add("Melon");

        salaryArray.add(123);
        salaryArray.add(145);
        salaryArray.add(1);
        salaryArray.add(234325);
        salaryArray.add(2345);

        List<String> newList = GenericSortingClass.sortArray(fruitArray);
        List<Integer> newListTwo = GenericSortingClass.sortArray(salaryArray);

        for (String entry : newList) {
            System.out.println(entry);
        }

        for (int entry : newListTwo) {
            System.out.println(entry);
        }

    }
}
