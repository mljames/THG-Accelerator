import java.util.List;

public class GenericSortingClass {

    public static <T extends Comparable<T>> List<T> sortArray(List<T> listToSort) {

        T temp;
        int n = listToSort.size();

        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (listToSort.get(j - 1).compareTo(listToSort.get(j)) > 0) {
                    temp = listToSort.get(j - 1);
                    listToSort.set(j - 1, listToSort.get(j));
                    listToSort.set(j, temp);
                }
            }
        }
        return listToSort;
    }
}
