## Spring Tutorial

Spring does a lot but it is most popular for dependency injection. What is dependency injection?

```java
Triangle myTriangle = new Triangle();
myTriangle.draw();

Circle myCircle = new Circle();
myCircle.draw();
```

But we desire polymorphism. Since both circle and triangle have a draw method we can create an interface called shape, from which both the circle and the triangle inherit. Now all we have to do:

```java
Shape shape = new Triangle();
shape.draw();

Shape shape = new Circle();
shape.draw();
```

This is polymorphism in action. The whole idea is that we don't know what object we have when we implment the method draw(). However this isn't true polymorphism because Triangle() and Circle() are still hard coded.

Now we're going to write a method which takes shape as a parameter and this method calls shape.draw().

```java
public void myDrawMethod(Shape shape) {
       shape.draw();
}
```

There has to be new code somewhere in this class where:

```java
Shape shape = new Triangle();
myDrawMethod(shape);
```

We're still tied to instantiating an object `new Triangle()`. To fix this we will have a drawing class:

```java
protected class Drawing {

	  private Shape shape;

	  public setShape(Shape shape) {
	  	 this.shape = shape;
		 }

	  public drawShape() {
	  	 this.shape.draw();
		 }
}
```

This class assumes that somebody is going to instantiate the shape object and provide it to the class. In a different class we'll be doing:

```java
Triangle myTriangle = new Triangle();
drawing.setShape(myTriangle);
drawing.drawShape();
```

We have separated the dependency from the drawing class. The drawing class does not own the relationship. The dependency that the drawing class has, the dependency to the triangle is actually injected by another class. This is the whole concept of dependency injection.

### Understanding Spring Bean Factory

Spring is a factory or container of beans.

TOMCAT is a servlet container, it reads the XML and identifies the servlets that need to be instantiated and then it creates those servlets.

Spring is a container, not of servlets but of beans. We can have a Spring container with as many objects as we want, and Spring manages and handles the instantiation of the objects, the lifecycle and the destruction of these objects. We are learning Spring in order for Spring to manage our object lifecycle.

Spring has an object called a Bean Factory. Instead of calling a `new` we reference the Bean factory which reads from a Spring XML which contains all our bean definitions (the blueprints) and the Bean factory creates a Bean from this blueprint making a new Spring Bean which is handed over back to the object. Since this new Bean has been created in the Bean factory, Spring knows about this Bean and therefore can manage the entire lifecycle of that bean.

```java
public class Triangle {

       private String type;
       private int height;

       public Triangle(String type) {
       	      this.type = type;
       }

       public Triangle(int height) {
       	      this.height = height;
       }
       
       public Triangle(String type, int height) {
       	      this.type = type;
	      this.height = height;
       }
       
       public void draw() {
       	      System.out.println(getType()+"Triangle Drawn of height" + getHeight());)
	      }

	public String getType() {
	    return type;
	}

	public int getHeight() {
	       return height;
	}

	public void setType(String type) {
	    this.type = type;
	}
}

import org.springframework.beans.factory.BeanFactory;

public class DrawingApp {

       public static void main(String[] args) {

       	      //Triangle triangle = new Triangle();
       	      //BeanFactory factory = new XmlBeanFactory(new FileSystemResource("spring.xml"));

	      ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
	      Triangle triangle = (Triangle) context.getBean("triangle");
	      triangle.draw();
	      
	      }
}
```

```xml
<beans>
	<bean id="triangle" class="org.koushik.javabrains.Triangle">
	<property name="type" value="Equilateral"/>"
	<constructor-arg value="Equilateral" />
	<constructor-arg type="int" value="20"/>
	<constructor-arg index="0"" value="Equilateral"/>
	<constructor-arg index="1" value="20"/>
	</bean>
</beans>
```

Why are we performing this circus act? Since it's a very simple task the benefits are not immediately obvious.

The property tag is called `setter` injection. Spring is using the setter in order to initialise the value with "equilateral". We can also use a `constructor injection`. Now if we comment out the `setter` and the `<property/>` it still sets the value equilateral. We can define as many constructor arguments as we want.

But what happens if we have an overloaded constructor? Since for Spring we input both values as strings we have a problem when we have overloaded constructors. We need to give Spring a clue therefore on what type the value you're passing is.

So we have two ways of dependency injection:

1) Setter injection
2) Constructor injection

Now we're going to try injection objects...

```java
public class Triangle {

       private Point pointA;
       private Point pointB;
       private Point pointC;

       public Point getPointA() {
       	      return pointA;
	}

	public void setPointA(Point pointA) {
	       this.pointA = pointA;
	}

	public Point getPointB() {
	       return pointB;
	}

	public void setPointB(Point pointB) {
	       this.pointB = pointB;
	}

	public Point getPointC() {
	       return pointC;
	}

	public Point setPointC() {
	       this.pointC = pointC;
	}

      	public void draw() {
	       System.out.println("PointA = " + getPointA().getX() + ", " + getPointA().getY() + ")");
	       System.out.println("PointB = " + getPointB().getX() + ", " + getPointB().getY() + ")");
	       System.out.println("PointC = " + getPointC().getX() + ", " + getPointC().getY() + ")");
      	}

}

public class Point {
       private int x;
       private int y;

       public int getX() {
       	      return x;
       }

       public int getY() {
       	      return y;
       }
}
```

```xml
<beans>

	<bean id="triangle" class="org.koushik.javabrains.Triangle">
	<property name="pointA" ref="zeroPoint"/>
	<property name="pointB" ref="point2"/>
	<property name="pointC" ref="point3"/>
	</bean>

	<bean id="zeroPoint" class="org.koushik.javabrains.Point">
	<property name="x" value="0"/>
	<propert name="y" value="0"/>
	</bean>

	<bean id="point2" class="org.koushik.javabrains.Point">
	<property name="x" value="-200"/>
	<propert name="y" value="0"/>
	</bean>

	<bean id="point3" class="org.koushik.javabrains.Point">
	<property name="x" value="20"/>
	<propert name="y" value="0"/>
	</bean>

</beans>
```

There's a whole load of initialisation here...

We have three Point objects that we have told Spring to create. We could do this in the drawingApp with `context.getBean`.

We then get the triangle bean which has properties pointA, pointB, pointC, and before it hands back the object it must resolve these dependencies first. These points have references to other beans. This is object nesting and Spring does all of this for us. Once the Triangle object is all created it returns the object back to the app which is making the reference to the `getBean`. We only need to `getBean` on the first object.

The good thing with this configuration is that we have one consolidated place where we have the map of the objects and the values that you need and we initialise everything with just one call to the getBean method.

### Inner beans, aliases and idref

If we say that `zeroPoint` has a general value that can be used throughout our package but that `point2` and `point3` have very specific values for the triangle. We will therefore move the bean defintions to inside the definition of the `triangle bean`.

```xml
<beans>
	<bean id="triangle" class="org.koushik.javabrains.Triangle" name="triangle-name">
	<property name="pointA">
		  <idref="zeroPoint>
	</property>
	<property name="pointB">
		<bean class="org.koushik.javabrains.Point">
			<property name="x" value="-200"/>
			<propert name="y" value="0"/>
		</bean>
	</property>
	<property name="pointC" ref="point3"/>
		<bean class="org.koushik.javabrains.Point">
			<property name="x" value="20"/>
			<property name="y" value="0"/>
		</bean>
	</property>
	</bean>

	<bean id="zeroPoint" class="org.koushik.javabrains.Point">
		<property name="x" value="0"/>
		<property name="y" value="0"/>
	</bean>

	<alias name="triangle" alias="triangle-alias"/>
</beans>
```

Spring aliases allows us to give different names to the same bean and that we can reference using aliases. We use the `<alias/>` tag.

### Initialising collections

* List
* Set
* Map

We'll now have in `class Triangle`:

```java
private List<Point> points;

public List<Point> getPoints() {
       return points;
}

public void draw() {
       for (Point point : points) {
       	   System.out.println("Point = (" + pointgetX() + ", "+point.getY() + ")");
       }
}
```

Now we have a list member variable.

```xml
<beans>

	<bean id="triangle" class="org.koushik.javabrains.Triangle">
	      <property name="points">
	      <list>
		<ref bean="zeroPoint"/>
		<ref bean="point2"/>
		<ref bean="point3"/>
	      </list>
	</bean>

	<bean id="zeroPoint" class="org.koushik.javabrains.Point">
	<property name="x" value="0"/>
	<propert name="y" value="0"/>
	</bean>

	<bean id="point2" class="org.koushik.javabrains.Point">
	<property name="x" value="-200"/>
	<propert name="y" value="0"/>
	</bean>

	<bean id="point3" class="org.koushik.javabrains.Point">
	<property name="x" value="20"/>
	<propert name="y" value="0"/>
	</bean>
</beans>
```

If the names of the beans (`id`) match the names of the member variables of the `Triangle` bean then we can add a tag `autowired="byName"` within the Triangle bean declaration. There are different ways we can autowire, for example `byType`, `constructor` etc..

### Spring bean scopes

There are two basic bean scopes in Spring:

1) **Singleton** - only once per Spring container - it is going to hand over the same bean instance on every call to the `getBean` in whatever class etc.
2) **Prototype** - new bean created with every request or reference - it works completely the other way to the Singleton pattern. New instances are created for every request and every reference.

If a bean is defined as a Singleton it gets the bean when the context is created but if a bean is defined as a prototype it gets the bean when `getBean` is called.

3) Web-aware context bean scopes
   * Request - new bean is created per servlet request.
   * Session - new bean is created per servlet session.
   * Global Session - New bean per global HTTP session.

Simply enter `scope="singleton/prototype"` in the bean definition.

### ApplicationContextAware

```java
public class Triangle implements ApplicationContextAware, BeanNameAware {

private ApplicationContext context = null;

@Override
public void setApplicationContext(ApplicationContext context) {
       throws BeansException {
       	      this.context = context;
	      }

@Override
public void setBeanName(String beanName) {
       System.out.println("Bean name is: "+beanName);
}

}
```

We can capture the application context therefore within the member variable of the class. What Spring does in when it loads all the beans it sees `implements ApplicationContextAware` it then performs the method `setApplicationContext(...)` and we get the context within the field of the class.

### Bean definition inheritance

We can have one bean that has all of the common defintions inside it and then we can inherit all of the bean definitions across all the other beans. It can be a bean itself or it can be abstract. Thus for example:

```xml
<beans>
	
	<bean id="parenttriangle" class="org.koushik.javabrains.Triangle">
	      <property name="pointA" ref="pointA"/>
	</bean>

	<bean id="parenttriangle" class="org.koushik.javabrains.Triange" parent="parenttriangle">
	      <property name="points">
	      <list>
		<ref bean="pointA"/>
	      </list>
	 </property>
	 </bean>

	<bean id="triangle1" class="org.koushik.javabrains.Triange" parent="parenttriangle">
	      <property name="points">
	      <list merge="true">
		<ref bean="pointB"/>
	      </list>
	 </property>
	 </bean>

	<bean id="triangle1" class="org.koushik.javabrains.Triangle" parent="parenttriangle">
		<property name="pointB" ref="pointB"/>
		<property name="pointC" ref="pointC"/>
	</bean>

	<bean id="triangle2" class="org.koushik.javabrains.Triangle">
		<property name="pointB" ref="pointB"/>
	</bean>

	<bean id="pointA" class="org.koushik.javabrains.Point">
	      <property name="x" value="0"/>
	      <propert name="y" value="0"/>
	</bean>

	<bean id="pointB" class="org.koushik.javabrains.Point">
	      <property name="x" value="-200"/>
	      <propert name="y" value="0"/>
	</bean>

	<bean id="pointC" class="org.koushik.javabrains.Point">
	      <property name="x" value="20"/>
	      <propert name="y" value="0"/>
	</bean>

</beans>
```

### Lifecycle Callbakcs

We can a method in our bean which gets when run when the bean is created as well as a method when the bean gets destroyed. 

```java
public class DrawingApp {

       public static void main(String[] args) {
       	      AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
	      context.registerShutdownHook();
	      Triangle triangle = (Triangle) context.getBean("triangle");
	      triangle.draw();
       }
      
}

public class Triangle implements InitializingBean, DisposableBean {


@Override
public void afterPropertiesSet() throws Exception {
       System.out.println("InitializingBean init method called for Triangle");
}

@Override
public void destroy() throws Exception {
       System.out.println("DisposableBean destroy method called for Triangle");
}

public void myInit() {
       System.out.println("My init method called for triangle");
}

public void cleanUp() {
       System.out.println("My cleanup method called for triangle");
}


init-method = "myInit", destroy-method = "cleanUp"
```

This interface tells Spring that my bean needs to know when it needs to be intialized. This interface has a method that needs to be overrided - `afterPropertiesSet()`. If we don't want to bind these special Spring interfaces to our code we can write two simple methods that are not specific to Spring themselves. 

If we have a standard for init and destroy methods we can configure them at a global level by:

```xml
<beans default-init-method="myInit" default-destroy-method="cleanUp">
```

### BeanPostProcessor

Classes that tell Spring that there is some processing Spring has to do after initializing a bean. This is a good method to extend the functionality of Spring. 

```java
public class DisplayNameBeanPostProcessor implements BeanPostProcessor {
       
       @Override
       public Object postProcessAfterInitialization(Object bean, String beanName)
       	      throws BeansException {
	      	     System.out.println("In After Initialization method. Bean name is "+beanName);
		     return bean;
}
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) 
	       throws BeansException {
	       	      System.out.println("In Before Initializer. Bean name is "+beanName);
		      return bean;
}	
}
```

Spring needs to know that we want this to be an action. Within the xml we therefore write:

```xml
<bean class="org.koushik.javabrains.DisplayNameBeanPostProcessor"/>
```

The code then runs before and after each and every bean is initialized.

### BeanFactoryPostProcessor

Very similiar to the BeanPostProcessor above, but it refers to initialising the Factory rather than each Bean.

We want to externalise the values of the bean outside of the spring xml.

```properties
pointA.pointX=0
pointA.pointY=0
```

We can add a placeholder to the spring.xml:

```xml
<bean id="pointA" class="org.koushik.javabrains.Point">
      <property name="x" value="${pointA.pointX}"/>
      <property name="y" value="${pointA.pointY}"/>
</bean>
```

In order to let Spring know that we want it to look inside the properties file to find these placeholder values we write:

```xml
<bean class="org.koushik.javabrains.MyBeanFactoryPP"/>
<bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
      <property name="locations" value="pointsconfig.properties"/>
</bean>
```

Spring must know where the .properties file is... We configure that in the xml as above.

### Coding to Interfaces

```java

public interface Shape {
       public void draw();
}

public class Triangle implements Shape {
       public void draw() {
       	      System.out.prntln("Drawing Triangle")
	      ...;
       }
}

public class Circle implments Shape {
       
       private Point center;       

       public void draw() {
       	      System.out.println("Deawing Circle");
       	      System.out.println("Circle: point is: ("+center.getX()+", "+center.getY()+")");
       }

       public Point getCenter() {
       	      return center;
	}

	@Required
	public void setCenter(Point center) {
	       this.center = center;
	 }
}
```

```xml
<bean id="circle" class="org.koushik.javabrains.Circle">
      <property name="center" ref="pointA"/>
</bean>
```

So instead in `DrawingApp`:

```java
public class DrawingApp {

       public static void main(String[] args) {
       	      ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
	      Shape shape = (Shape) context.getBean("shape");
	      shape.draw();
	      }
}
```

### Introduction to Annotations and the Required Annotation

If we want to validate that all of the required dependencies are met then we can do that with the required annotation. If we look at the `Circle` class above, we mark the setter method with @Required - we want the exception to be thrown when the application is being set up. We need to define a BeanPostProcessor which validates the @Required annotation. We therefore just need to define the bean in the spring.xml:

```xml
<bean class="org.springframework.beans.factory.annotation.RequiredAnnotationBeanPostProcessor"/>
```

We essentially get more useful feedback than what we get with a NullPointerException."

### The Autowired Annotation

@Autowired - Spring looks for type first and then it looks for name to inject a bean into a parent bean. Finally it looks for qualifier within the xml. @Autowired is put in front of the setter method as normal. We can use:

```java
@Autowired
@Qualifier("circleRelated")
public void setCenter(Point center) {
       this.center = center;
}
```
```xml
<bean id="pointA" class="org.koushik.javabrains.Point">
      <qualifier value="circleRelated"/>
      <property name="x" value="0"/>
      <property name="y" value="0"/>
</bean>

<bean class="org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor"/>
<context:annotation-config/>
```

### Some JSR-250 Annotations

1) `@Resource(name="pointC")` - injects dependencies into member variables of a class (specific to Java, not Spring)
2) `@PostConstruct` (again specific to Java, as is @PreDestroy)
3) `@PreDestroy`

### Component and Stereotype Annotations

We need to tell Spring what classes of ours are going to be Beans and which are not. This saves us from having to put them in a spring.xml file. This is done via an `@Component` tag.

```java
@Component
public class Circle implements Shape {
...
}
```

With an xml though we can use the same class and define three different beans. It is not possible to have three different behaviours which arise from using annotations. We can use the same class and use different beans with different metadata to produce different behaviour.

We need to ask Spring to scan through all our classes to find our annotations and mark certain classes. We therefore need to add:

```xml
<context:component-scan base-package="org.koushik.javabrains"/>
```

There are other annotations to `@Component` we can use. For example `@Service` which gives Spring extra information that this class is a service. Other annotations include `@Controller`, `@Repository`. `@Component` is the more generic bean form.

These annotations act as a layer of documentation.
