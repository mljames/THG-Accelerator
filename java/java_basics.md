## Java Course Notes

Code to Compile -> Compiler -> Byte Code -> Java Virtual Machine

We only need to build once but we can run anywhere without any dependence (dependencies may include OS and Architecture).

* **JVM** - Java Virtual Machine, which converts bytecode into machine code
* **JRE** - Java Runtime Environment, which instantiates JVM to run a Java program
* **JDK** - Java Development Kit, which has tools for compilation, debugging etc.
* **JIT** - Just In Time, which optimises runtime

The philosophy of Java is WORA - write once, run everywhere.

Java memory areas:

* Heap
* Stack

The primitive data types in Java are:
* boolean
* byte
* char
* short
* int
* long
* float
* double

An object is an entity that has a state (defined by fields) and a behaviour defined by method calls. We can define the state and behaviour of an object by using a template called a class. So a class consists of `Class Fields` and `Class Methods`.

Java modifiers are the keywords that change the scope/meaning of a declared entity:

* Access Modifiers
  * Set the access levels for classes, variables, methods and constructors
  * e.g. `private` (within a class), `protected` (within a package plus inheritance), `package` (within a package), `public` (anywhere)
* Non-access Modifiers
  * Non-access modifier are used to achieve a specific functionality
  * `static` - only one copy of the staic variable exists regardless of the number of instances of the class
  * `final` - a final variable can be explicitly intialized only once

Java Class Fields and Methods:

Class Field - `<Access Modifier><Optional Non-Access modifier><Data Type><FieldName>`

Class Method - `<Access Modifier><Optional Non-Access modifier><Return Type><Name><Optional Parameters>`

For example:

```java
private static int maxSize = 10;

public int returnMaxSiize (int maxSize) {
       return maxSize;
}
```

N.B. `void` is used for a method that doesn't return anything.

Java Package:

* A namespace to organise classes into different folders
* Used to identify separation of concerns
* Convention - com.thg.model, com.thg.view etc.

```java
public class HelloWorld {

public int initialValue = 2;
public String initialString = "myFirstString";

public void myFirstFunction() {
    System.out.println(" Hello World, I am Batman")
    }
}
```

Object Creation in Java has the following components:

* Declaration
* Instantiation
* Initialisation

```java
public class mySecondClass {
HelloWorld myObject = new HelloWorld();
}
```

The JVM looks for the `main()` method to start executing a Java program.

```java
public class MainClass {

public static void main(String[]args) {

    }
}
```

Java Constructors:

Thy have one purpose; to create instances of a class - they are not like other methods e.g. they do not have a return type (not even void). They cannot be used with non-acces modifiers i.e. static, final, abstract, synchronized.

```java
public class MySecondClass {
    public MySecondClass(){}
    }

MySecondClass obj = new MySecondClass();
```

Java Control Statements:

Inheritance:

* Can be identified by an `is` a relationship e.g. Mountain Bike is a Bike. Mountain Bike is called a child of Bike and Bike is supercall of Mountain Bike. We use the word `extends` for this parent-child relationship.

```java
public class MountainBike extends Bike {
       private int yearsofExtendedWarranty;
       public MountainBike() {
       	      super();
	      this.yearsOfExtendedWarranty = 10;
	}
}
```

Java supports single inheritance, multilvel inheritance, hierarchical inheritance - multiple inheritance is not supported.

We have `constructor overloading`, `method overloading`, `method overriding` (method overriding relates to a child overriding the parent method with the same name).

Logical short circuiting - A short circuit oprerator is one that stops evaluating arguments when truth value is determined.

Java data types:

* Arrays
  * An array can hold a fixed number of data entities or the same type. Each item in an array is called an element and every element can be accessed by its index only.

```java
public class ArrayDemo {
       private int[] myArray = new int[5];
}
```

* Lists
  * Popular implementations are ArrayList and LinkedList - ArrayList is a good candidate for lots of reads but for lots of writes LinkedList should be used.

```java
public class ListDemo {
       List<Integer> intList = new ArrayList<>();
}
```

* Map
  * Association of keys with values
    * get(Object o)
    * put(K k, V value)
    * containsKey(Object k)
  * HashMap is unsynchronized and permits nulls - makes no guarentee on the order of output. We use a HashTable to store key, value pairs for the HashMap. HashMap will make decisions on the size of the HashTable used.


```java
HashMap<Integer, String> myMap = new HashMap<Integer, String>();
myMap.put(1,"Khan");
```

* Set
  * Unordered collection of elements where there ccan be at most one of each.
    * add(Element e)
    * contains(Element e)
    * remove(Element e)
    * size()
  * HashSet internally uses HashMap to store entities.
