package com.thg.accelerator.activemq.routes;

import com.thg.accelerator.activemq.consumer.ProductAddListener;
import com.thg.accelerator.activemq.event.ProductAddEvent;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SimpleRouteBuilder extends SpringRouteBuilder {

  public static final String PRODUCT_ADD_LISTENER_ENDPOINT = "product_add_listener_endpoint";

  @Value("${incoming.product.add.uri}")
  private String productAddUri;

  @Override public void configure() throws Exception {
    getContext().addEndpoint(PRODUCT_ADD_LISTENER_ENDPOINT, endpoint(productAddUri));
    from(PRODUCT_ADD_LISTENER_ENDPOINT)
        .unmarshal().json(JsonLibrary.Jackson, ProductAddEvent.class)
        .bean(new ProductAddListener(), "productAdded");
  }
}
