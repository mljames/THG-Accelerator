package com.thg.accelerator.activemq;

import com.thg.accelerator.activemq.domain.Product;
import com.thg.accelerator.activemq.producer.ProductAddProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class App {

  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }

  @Autowired
  private ProductAddProducer productAddProducer;

  @Bean
  public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
    return args -> {
      while (true) {
        Product product = new Product("product_1", "a_new_product", 10);
        productAddProducer.productAdded(product);
        Thread.sleep(10 * 1000);
      }
    };

  }
}
