package com.thg.accelerator.activemq.event;

import java.util.Objects;

public class ProductAddEvent {

  private String productName;

  public ProductAddEvent() {
  }

  public ProductAddEvent(String productName) {
    this.productName = productName;
  }

  public String getProductName() {
    return productName;
  }

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProductAddEvent that = (ProductAddEvent) o;
    return Objects.equals(productName, that.productName);
  }

  @Override public int hashCode() {
    return Objects.hash(productName);
  }

  @Override public String toString() {
    return "ProductAddEvent{" +
        "productName='" + productName + '\'' +
        '}';
  }
}
