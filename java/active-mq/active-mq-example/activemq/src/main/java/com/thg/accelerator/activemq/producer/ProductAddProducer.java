package com.thg.accelerator.activemq.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.thg.accelerator.activemq.domain.Product;
import com.thg.accelerator.activemq.event.ProductAddEvent;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductAddProducer {

  @Value("${outgoing.product.add.uri}")
  private String outgoingAddUri;

  private ProducerTemplate template;
  private ObjectMapper objectMapper;
  private CamelContext camelContext;

  @Autowired
  public ProductAddProducer(ProducerTemplate template,
      ObjectMapper objectMapper, CamelContext camelContext) {
    this.template = template;
    this.objectMapper = objectMapper;
    this.camelContext = camelContext;
  }

  public void productAdded(Product product) {
    sendEvent(outgoingAddUri, new ProductAddEvent(product.getProductName()));
  }

  private void sendEvent(String uri, ProductAddEvent event) {
    try {
      template.sendBody(camelContext.getEndpoint(uri),
          objectMapper.writeValueAsString(event));
    } catch (JsonProcessingException e) {
      throw new RuntimeException("Couldn't marshal event: ");
    }
  }
}