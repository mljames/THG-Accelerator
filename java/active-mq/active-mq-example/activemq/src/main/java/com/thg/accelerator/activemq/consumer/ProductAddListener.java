package com.thg.accelerator.activemq.consumer;

import com.thg.accelerator.activemq.event.ProductAddEvent;
import org.springframework.stereotype.Component;

@Component
public class ProductAddListener {
  public void productAdded(ProductAddEvent productEvent) {
    System.out.println("Product with product number: " + productEvent.getProductName());
  }
}
