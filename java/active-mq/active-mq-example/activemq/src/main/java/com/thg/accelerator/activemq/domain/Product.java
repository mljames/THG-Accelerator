package com.thg.accelerator.activemq.domain;

import java.util.Objects;

public class Product {

  private String productName;
  private String productDescription;
  private int cost;

  public Product(String productName, String productDescription, int cost) {
    this.productName = productName;
    this.productDescription = productDescription;
    this.cost = cost;
  }

  public String getProductName() {
    return productName;
  }

  public String getProductDescription() {
    return productDescription;
  }

  public int getCost() {
    return cost;
  }

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Product product = (Product) o;
    return cost == product.cost &&
        Objects.equals(productName, product.productName) &&
        Objects.equals(productDescription, product.productDescription);
  }

  @Override public int hashCode() {
    return Objects.hash(productName, productDescription, cost);
  }

  @Override public String toString() {
    return "Product{" +
        "productName='" + productName + '\'' +
        ", productDescription='" + productDescription + '\'' +
        ", cost=" + cost +
        '}';
  }
}