### ActiveMQ with Camel

1. Dependencies

* camel-core - basic module of apache camel
* camel-stream - used to send output to the console
* camel-jms & activemq-camel - ActiveMQ JMS components
* spring-context & camel-spring - configuring camel context in Spring
* slf4j-api & slf4j-log4j12 - logging

2. ActiveMQ component's URI format

** activemq:[queue:|topic:]destinationName **

destinatinoName is an ActiveMQ queue or topic name.

3. Configuring JMS

The first thing to do before starting activeMQ is to create a connection factory, to connect to an embedded broker.

`ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost");`

It is expensive to open up a connection to an ActiveMQ broker so it is recommended to pool the connections.

4. Connection Pool

