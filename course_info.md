# Accelerator Course Info

## Term 1 - Core Skills

### Fundamentals of Programming

**Text:** *Introduction to Computation and Programming Using Python: With Application to Understanding Data Second Edition by John Guttag*
**Open Courseware (OCW):** *Introduction to Computer Science and Programming in Python EDX-MIT_6.000.1 (or MIT 6.000.1)*
**Supplemental Text:** *SICP in Python - https://www.gitbook.com/book/wizardforcel/sicp-in-python/details*

* It's all files
* Elements of code
* Basic programs
* Algorithms

### Computer System Architecture

**Text:** *Computer Architecture: A Quantitative Approach by Hennessy and Patterson*
**OCW:** *Computer System Architecture MIT 6.823*

* Internals of the computer, CPU logic, I/O, etc
* Networking
* Memory
* Storage (internal and external)
* Operating systems
* Resources, allocations, libraries, scheduling
* Distributed and parallel systems
* Virtualization

### Databases

**Text:** *First Course in Database Systems by Ullman and Widom*
**OCW:** *Databases MIT 6.830*

* Relational DBs & DBMS - storage, structures, manipulation, querying
* NoSQL DBs
* Parallel and distributed designs

### Operating Systems

**Text:** *Operating Systems by Deitel, Deitel, Choffnes*
**OCW:** *Operating Systems Stanford CS140*
**Supplemental Text:** *Stevens, Advanced Programming in the Unix Environment, 3rd.Edition*

* Intro
* Files, I/O, Filesystem, Stdio and Buffering
* Processes, Process Control, Process Relationships
* Signals
* Threads and Thread Control
* Daemons
* Advanced IO (Async, Nonblocking, Multiplexing)
* IPC and Network IPC (pipes, queues, semaphores, sockets)

## Term 2 - Advanced Skills

### Object Oriented Programming in Java

**Text:** *Introduction to Programming in Java by Robert Sedgewick*
**OCW:** *Introduction to Programming in Java MIT 6.092*

* Focus on OOP
* System analysis and design using OO
* Modeling systems with OO
* Turning requirements into OO design
* Non coupled systems
* Classes, objects, packages
* Primitive data types, variables, expressions, methods, parameters, control
* Implementation
* Testing
* Limitations
* GUIs - original OOP use case
* Learn specific language use better
* Specific syntax, semantics, data structures
* APIs
* Threading
* Etc

### Algorithms, Higher Order Data Structures

*Included in the Python material*

**Text:** *Algorithms by Sedgewick*

* Core algorithms
* Big-O and asymptotic behaviour
* Sorting
* Searching
* Linked structures
* Trees
* Hashing
* Graphs
* Complexity theory

### Networks and Communications

**Text:** *Computer Networks: A Top-Down Approach by Kurose and Ross*
**OCW:** *Introduction to Computer Networking Stanford CS144*
**Supplemental text:** *Tanenbaum, Computer networks*

* Networking concepts and the OSI model
* TCP/IP and Ethernet
* Addressing and routing protocols
* Switches, bridges, and routers
* LANs, WANs, and WAN protocols
* Mobile and cell communications
* Networking security
* Web protocols (Layer 7)
* Streaming

### Software Engineering

*Practice based*

**Text:** *Sommerville, Software Engineering*

* Definition, design, analysis, verification
* Requirements, coding, testing, quality, monitoring
* Design patterns, anti-patterns (and the reasons that OOP needs crutches :)
* Ivory tower, shortest path, tech debt
* Development lifecycle
* On the job rather than course: agile, CI, project management

### Web Based Development

* HTML, CSS, Javascript, JSON
* Web related networking core concepts
* Build web apps/server, possibly as a group project
* Security

### Compilers

*Included in above material*

**Text:** *Compilers: Principles, Techniques, and Tools by Aho, Lam, Sethi and Ullman*
**OCW:** *Compilers Stanford CS143*

* Lexical Analysis, Regex, Finite State Machines
* Parsing, Syntax, Grammar
* Translation and Code Generation
* Runtime Environments
* Optimization

### Cyber Security

**Text:** *Cryptography and Network Security: Principles and Practice by Stallings*
**OCW:** *Cyber Security Stanford CS155*

* Encryption, hashing (HTTPS case study?)
* Common security problems and programming that leads to them
* OWASP top 10
* Defensive programming
* Cryptography
* Networking considerations
* Information management

### Data Science

**Text:** *Introduction to Computation and Programming Using Python: With Application to Understanding Data Second Edition by John Guttag (Data science portion)*
**OCW:** *Introduction to Computational Thinking and Data Science MIT 6.000.2*
**Supplemental Texts:** *An Introduction to Statistical Learning (G James, et al); Doing Data Science, straight talk from the front line (O'Neil, Schult); Big Data for Beginners (less mathematical introduction)*

* Statistics for data science
* Volume, velocity, variety
* Data storage, manipulation
* Data analysis: regression, logistic regression, linear models, non parametric models, multilevel and hierarchical models
* Data engineering
* Data visualization
* Machine learning basics and applied
* A/B testing
* Hypothesis testing
* Designing and understanding tests
* Causality
* R, Numpy, Spark,

#### Topical Advanced Concepts

* Blockchain
* Artificial Intelligence
* Quantum computing
* Natural language processing