from trees import BinarySearchTree

class set_operations(BinarySearchTree):
    def __init__(self):
        BinarySearchTree.__init__(self)
    def __contains__(self,value):
        return self.find_node(value)
    def __or__(self,other):
        set3 = set_operations()
        for i in self:
            set3.insert_node(i)
        for j in other:
            set3.insert_node(j)
        return set3
    def __and__(self,other):
        set3 = set_operations()
        for i in self:
            if i in other:
                set3.insert_node(i)
        return set3
    def from_iterable(self, iter):
        for element in iter:
            self.add(element)
    def __str__(self):
        return ", ".join([str(i) for i in self])

# Populate sets

set1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
set2 = {1, 3, 5, 7, 9, 11}

set_1 = set_operations()
set_2 = set_operations()
for elem in set1:
    set_1.insert_node(elem)
for elem in set2:
    set_2.insert_node(elem)

print(set_operations.__or__(set_1,set_2))
