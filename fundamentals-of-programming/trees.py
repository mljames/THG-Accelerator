# This first example is a simple binary tree

class BinaryTree:
    def __init__(self, value):
        self.value = value
        self.left_child = None
        self.right_child = None

    def insert_left(self, value):
        if self.left_child == None:
            self.left_child = BinaryTree(value)
        else:
            new_node = BinaryTree(value)
            new_node.left_child = self.left_child
            self.left_child = new_node
    def insert_right(self, value):
        if self.right_child == None:
            self.right_child = BinaryTree(value)
        else:
            new_node = BinaryTree(value)
            new_node.right_child = self.right_child
            self.right_child = new_node

# This second example is a binary search tree

class Node():
    def __init__(self,value):
        self.value = value
        self.left_child = None
        self.right_child = None
class BinarySearchTree():
    def __init__(self):
        self.root = None
    def insert_node(self,value):
        if self.root == None:
            self.root = Node(value)
        else:
            self._insert_node(value, self.root)
    def _insert_node(self, value, cur_node):
        if value < cur_node.value:
            if cur_node.left_child == None:
                cur_node.left_child = Node(value)
                cur_node.left_parent = cur_node
            else:
                self._insert_node(value,cur_node.left_child)
        elif value > cur_node.value:
            if cur_node.right_child == None:
                cur_node.right_child = Node(value)
                cur_node.right_child.parent = cur_node
            else:
                self._insert_node(value,cur_node.right_child)
        else:
            print("Value already in tree")
            pass
    def find_node(self, value):
        if self.root != None:
            return self._find_node(value, self.root)
        else:
            return None
    def _find_node(self, value, cur_node):
        if value == cur_node.value:
            return cur_node
        elif value < cur_node.value and cur_node.left_child != None:
            return self._find_node(value,cur_node.left_child)
        elif value > cur_node.value and cur_node.right_child != None:
            return self._find_node(value,cur_node.right_child)
        else:
            return None
    def __iter__(self, branch=None):
        branch = branch or self.root
        if branch.left_child:
            yield from self.__iter__(branch.left_child)
        yield branch.value
        if branch.right_child:
            yield from self.__iter__(branch.right_child)

# This third example is an AVL tree - this specifies a balancing operation

class node:
    def __init__(self,value):
        self.value = value
        self.left_child = None
        self.right_child = None
        self.parent = None
        self.height = 1

class AVLTree:
    def __init__(self):
        self.root = None
    def insert(self, value):
        if self.root == None:
            self.root = node(value)
        else:
            self._insert(value,self.root)
    def _insert(self,value,cur_node):
        if value < cur_node.value:
            if cur_node.left_child == None:
                cur_node.left_child = node(value)
                cur_node.left_parent = cur_node
                self._inspect_insertion(cur_node.left_child)
            else:
                self._insert(value,cur_node.left_child)
        elif value > cur_node.value:
            if cur_node.right_child == None:
                cur_node.right_child = node(value)
                cur_node.right_child.parent = cur_node
                self._inspect_insertion(cur_node.right_child)
            else:
                self._insert(value,cur_node.right_child)
        else:
            print("Value already in tree!")
    def height(self):
        if self.root != None:
            return self._height(self.root,0)
        else:
            return 0
    def _height(self,cur_node,cur_height):
        if cur_node == None:
            return cur_height
        left_height = self._height(cur_node.left_child,cur_height+1)
        right_height = self._height(cur_node.right_child,cur_height+1)
        return max(left_height,right_height)
    def find(self,value):
        if self.root != None:
            return self._find(value,self.root)
        else:
            return None
    def _find(self,value,cur_node):
        if value == cur_node.value:
            return cur_node
        elif value < cur_node.value and cur_node.left_child != None:
            return self._find(value,cur_node.left_child)
        elif value > cur_node.value and cur_node.right_child != None:
            return self._find(value,cur_node.right_child)
    def delete_value(self,value):
        return self.delete_node(self.find(value))
    def delete_node(self,node):
        if node == None or self.find(node.value) == None:
            print("Node to be deleted not found in tree!")
            return None
        children = 0
        if node.left_child:
            children += 1
        if node.right_child:
            children += 1
        if children == 0:
            if node.parent != None:
                if node.parent.left_child == node:
                    node.parent.left_child = None
                else:
                    node.parent.right_child = None
        if children == 1:
            child = node.left_child if node.left_child != None else node.right_child
            if node == self.root:
                self.root = child
                self.root.parent = None
            else:
                node.parent.left_child == None
        if children == 2:
            smallest=node.right_child
            while smallest.left_child != None:
                smallest = smallest.left_child
            node.value = smallest.value
            self.delete_node(smallest)
        if node.parent != None:
            node.parent.height = 1 + max(self.get_height(node.parent.left_child),self.get_height(node.parent.right_child))
            self._inspect_deletion(node.parent)
    def search(self,value):
        if self.root!=None:
            return self._search(value,self.root)
        else:
            return False
    def _search(self,value,cur_node):
        if value==cur_node.value:
            return True
        elif value < cur_node.value and cur_node.left_child != None:
            return self._search(value,cur_node.left_child)
        elif value > cur_node.value and cur_node.right_child != None:
            return self._search(value,cur_node.right_child)
        return False

    # Next follow functions specific for AVL, that is to say functions for re-balancing

    def _inspect_insertion(self,cur_node,path=[]):
        if cur_node.parent == None:
            return
        path = [cur_node]+path
        left_height = self.get_height(cur_node.parent.left_child)
        right_height = self.get_height(cur_node.parent.right_child)
        if abs(left_height - right_height) > 1:
            path = [cur_node.parent]+path
            self._rebalance_node(path[0],path[1],path[2])
            return
        new_height = 1 + cur_node.height
        if new_height > cur_node.parent.height:
            cur_node.parent.height = new_height
        self._inspect_insertion(cur_node.parent,path)
    def _inspect_deletion(self,cur_node):
        if cur_node == None:
            return
        left_height = self.get_height(cur_node.left_child)
        right_height = self.get_height(cur_node.right_child)
        if abs(left_height - right_height) > 1:
            y = self.taller_child(cur_node)
            x = self.taller_child(y)
            self._rebalance_node(cur_node,y,x)
            self._inspect_deletion(cur_node.parent)
    def _rebalance_node(self,z,y,x):
        if y == z.left_child and x == y.left_child:
            self._right_rotate(z)
        elif y == z.left_child and x == y.right_child:
            self._left_rotate(y)
            self._right_rotate(z)
        elif y == z.right_child and x == y.right_child:
            self._left_rotate(z)
        elif y == z.right_child and x == y.left_child:
            self._right_rotate(y)
            self._left_rotate(z)
        else:
            raise Exception('_rebalance_node: z,y,x node configuration not recognized!')
    def _right_rotate(self,z):
        sub_root = z.parent
        y = z.left_child
        t3 = y.right_child
        y.right_child = z
        z.parent = y
        z.left_child = t3
        if t3!=None:
            t3.parent = z
        y.parent = sub_root
        if y.parent == None:
            self.root = y
        else:
            if y.parent.left_child == z:
                y.parent.left_chid=y
            else:
                y.parent.right_child=y
        z.height = 1 + max(self.get_height(z.left_child),self.get_height(z.right_child))
        y.height = 1 + max(self.get_height(y.left_child),self.get_height(y.right_child))
    def _left_rotate(self,z):
        sub_root = z.parent
        y = z.right_child
        t2 = y.left_child
        y.left_child = z
        z.parent = y
        z.right_child = t2
        if t2 != None:
            t2.parent = z
        y.parent = sub_root
        if y.parent == None:
            self.root = y
        else:
            if y.parent.left_child == z:
                y.parent.left_child = y
            else:
                y.parent.right_child = y
        z.height = 1 + max(self.get_height(z.left_child),self.get_height(z.right_child))
        y.height = 1 + max(self.get_height(y.left_child),self.get_height(y.right_child))
    def get_height(self,cur_node):
        if cur_node == None:
            return 0
        return cur_node.height
    def taller_child(self,cur_node):
        left = self.get_height(cur_node.left_child)
        right = self.get_height(cur_node.right_child)
        return cur_node.left_child if left >= right else cur_node.right_child

#a = AVLTree()
#for i in range(10):
    #print("Inserting %d"%i)
    #a.insert(i)
    #print(a)
#for i in range(10):
    #print("Deleting %d"%i)
    #a.delete_value(i)
    #print(a)
