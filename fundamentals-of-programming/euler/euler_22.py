# Euler 22

import string
import time
start_time = time.time()

dictionary = dict(zip(string.ascii_lowercase,range(1,27)))

def score():
    f = open('sorted.txt','r')
    score = 0
    counter = 1
    for line in f:
        for l in line:
            if l not in dictionary:
                continue
            score += counter*dictionary[l]
        counter += 1
    return score

print(score())
print("--- %s seconds ---" % (time.time() - start_time))
