# Even Fibonacci Numbers

count = 0
down1 = 0
down2=0
current = 1
while current < 4000001:
    down2 = down1
    down1 = current
    current = down1 + down2
    if current%2 == 0:
        count = count + current
    else:
        count = count
        
print(count)
