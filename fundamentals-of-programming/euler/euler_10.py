# Summation of primes

import numpy as np
import time
start_time = time.time()

def main(up_to):

    primes = [2]
    sum_ = 0

    for i in range(3,up_to,2):
        for p in primes:
            if i%p == 0:
                break
        else:
            primes = np.append(primes,i)
        
    return print(sum(primes))

main(200000)
print("--- %s seconds ---" % (time.time() - start_time))



            
            