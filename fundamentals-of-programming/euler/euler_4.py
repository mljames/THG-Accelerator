# Largest palindrome product

import numpy as np

def is_pallindrome(num):
    if str(num) == str(num)[::-1]:
        return True
    else:
        return False
    
def multiply(x,y):
    return x*y

def pallindrome_finder(x,y):
    num = multiply(x,y)
    if is_pallindrome(num):
        return True
    else:
        return False
    
pallindrome = []
result = []
for i in range(1000,100,-1):
    for j in range(1000,100,-1):
        if pallindrome_finder(i,j):
            result = np.append(result,i*j)
            
print(int(max(result)))      
