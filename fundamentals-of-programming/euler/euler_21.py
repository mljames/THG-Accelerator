# Euler 21

import numpy as np
import time
start_time = time.time()

def factor(num):
    factor_set = set()
    factor_set_2 = set()
    for i in range(1,int(np.sqrt(num))+1):
        if num%i == 0:
            factor_set.add(i)
            factor_set.add(num//i)
    factor_set.remove(num)
    first = int(sum(factor_set))
    for i in range(1,int(np.sqrt(first))+1):
        if first%i == 0:
            factor_set_2.add(i)
            factor_set_2.add(first//i)
    factor_set_2.remove(first)
    second = int(sum(factor_set_2))
    if num == second and first != second:
        return num
    else:
        return 0

sum_ = 0
num = 2
while num<10000:
    sum_ += int(factor(num))
    num += 1
print(sum_)
print("--- %s --- seconds" % (time.time()-start_time))
