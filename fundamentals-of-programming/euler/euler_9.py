# Special Pythagorean triplet

def pythagorean_triplet(a,b,c):
    if a**2 + b**2 == c**2:
        return True

for i in range(1,1001):
    for j in range(1,1001):
        if pythagorean_triplet(i,j,(1000-i-j)) and 1000 - i -j > 0:
            print(i*j*(1000-i-j))
            break

