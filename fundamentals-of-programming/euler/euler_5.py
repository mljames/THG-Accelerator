# Smallest multiple (slow)

import math

def divisible(x,n):
    for i in range(1,n+1):
        if x%i != 0:
            return False
    return True

def LCM(n):
    for i in range(2520,math.factorial(n),n):
        if divisible(i,n):
            break
    return i

print(LCM(20))
