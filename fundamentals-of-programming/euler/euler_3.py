# Largest prime factor

import numpy as np
array = []
number = int(input('Enter a number: '))
for num in range(1,int(np.sqrt(number))):
    if number%num == 0:
        count = 0
        for i in range(1,num+1):
            if num%i == 0:
                count = count + 1
        if count == 2:
            array = np.append(array,num)         
print('Largest prime factor is ', max(array))