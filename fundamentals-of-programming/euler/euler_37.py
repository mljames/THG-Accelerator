# Euler 37
import numpy as np
import time
start_time = time.time()

def is_prime(num):
    for i in range(2,int(np.sqrt(num)+1)):
        if num%i == 0:
            return False
    if num == 1:
        return False
    else:
        return True
    
def rearrange_string(num):
    string = str(num)
    length = len(string)
    array = []
    for i in range(length):
        if i != 0:
            array.append(string[:-i])
        array.append(string[i:])
    return array

def are_prime(array):
    for el in array:
        if is_prime(int(el)):
            continue
        else:
            return False
    return True

primes_ = []

def gen_primes(num):
    primes = [True] * (num + 1)
    primes[0] = False
    primes[1] = False
    primes_list = []
    for p in range(2,int(np.sqrt(len(primes))+1)):
        if primes[p]:
            j = p*2
            while j <= num:
                primes[j] = False
                j += p
    for i in range(len(primes)):
        if primes[i]:
            primes_list.append(i)
    return primes_list

#print(gen_primes(10000))

ind = 5
prime = gen_primes(1000000)
while len(primes_) < 11:
    x = rearrange_string(prime[ind])
    if are_prime(x):
        primes_.append(prime[ind])
        ind += 1
    else:
        ind += 1
print(primes_,sum(primes_))
print("--- %s --- seconds" % (time.time()-start_time))
