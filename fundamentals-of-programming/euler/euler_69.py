# Euler 69
import numpy as np
import math
import time
start_time = time.time()

def prime(n):
    prime = [True] * (n+1)
    for i in range(2,math.ceil(np.sqrt(n))+1):    
        if prime[i]:
            j = i*2
            while j <= n:
                prime[j] = False
                j += i
    return prime

def phi(num,prime_list):
    primes = prime_list
    phi = 1
    for n in range(2,math.ceil(np.sqrt(num+1))+1):
        if prime_list[n] and num%n == 0:
                phi *= 1 - (1/n)
    phi = num * phi
    ans = num / phi
    return num, ans

primes = prime(1000000)
y_before = 0
n = 2
while n < 1000000:
    x, y = phi(n, primes)
    if y_before < y:
        y_before = y
        answer = x
    n += 1

print('The value of n is '+str(answer))
print("--- %s seconds ---" % (time.time() - start_time))
