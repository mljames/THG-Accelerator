# what is the 10001st prime

# the idea here is that every number can be reduced to a product of primes

import numpy as np

def nthprime(n):
    primes = [2,3,5,7,11]
    i = 12
    while len(primes) < n:
        for p in primes:
            if i%p == 0:
                i += 1
                break
        else:
            primes = np.append(primes,i)
            i += 1
    return print(primes[n-1])

nthprime(10001)
            
