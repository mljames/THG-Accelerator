# Euler 41

import numpy as np
import time
start_time = time.time()
def get_permutations(sequence):
    if len(sequence) <= 1:
        return [sequence]
    perms = []
    for n in range(len(sequence)):
        remaining_elements = sequence[:n] + sequence[(n+1):]
        z = get_permutations(remaining_elements)
        for t in z:
            perms.append(sequence[n]+t)
    return perms

def is_prime(num):
    for i in range(2,int(np.sqrt(num))+1):
        if num%i == 0:
            return False
    return True

string = '1234567890'
def pandigital(string):
    permute = get_permutations(string)
    primes = []
    for p in permute:
        if is_prime(int(p)):
            primes.append(p)
    if len(primes) >= 1:
        return max(primes)
    string = string[:-1]
    return pandigital(string)

print(pandigital(string))
print("--- %s --- seconds" % (time.time()-start_time))
