# Project Euler 12

import numpy as np

start_time = time.time()

def find_divisors(n):
    num_divisors = set()
    for p in range(1,int(np.sqrt(n)+1)):
        if n%p == 0:
            num_divisors.add(p)
            num_divisors.add(n // p)
    return num_divisors

def triangle(n):
    return (n*(n+1))/2

n = 1
while len(find_divisors(triangle(n))) < 500:
   n += 1
print(int(triangle(n)))
