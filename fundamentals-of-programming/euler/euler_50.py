# Euler 50

import numpy as np
import time
start_time = time.time()

def is_primes(num):
    primes = [True] * (num+1)
    primes[0] = False
    primes[1] = False
    for p in range(2,int(np.sqrt(num)+1)):
        if primes[p] ==  True:
            j = p*2
            while j <= num:
                primes[j] = False
                j += p
    primes_list = []
    count = 0
    for prime in primes:
        if prime:
            primes_list.append(count)
        count += 1
    prime_addition = []
    for i in range(0,len(primes_list)-21):
        x = np.sum(primes_list[i:(i+21)])
        print(x)
        if x < num and x in primes_list:
            prime_addition.append(x)
    return max(prime_addition)

print(is_primes(1000))
print("--- %s --- seconds" % (time.time()-start_time))
