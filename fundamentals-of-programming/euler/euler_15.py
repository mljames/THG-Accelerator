# Euler 15

def factorial(n):
    if n == 1:
        return 1
    return(n * factorial(n-1))

# Constraint that a move may only be down and right constrains factorial such that its 2n! divided by n! * n!

def answer(n):
    return factorial(n*2)/(factorial(n)**2)

print(answer(20))
