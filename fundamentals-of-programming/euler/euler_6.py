# Difference of squares

def sum_squares_diff(n):
    total_sum = 0
    sum_squares = 0
    for i in range(1,n+1):
        total_sum += i
        sum_squares += i**2
    return print('The difference of sqaures for the first ' + str(n) +' natural numbers is '+str(((total_sum)**2 -sum_squares)))
        
sum_squares_diff(100)