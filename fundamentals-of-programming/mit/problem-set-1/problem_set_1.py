# Problem Set 1

# Part A

total_cost = input('What is the cost of your dream home? ')
portion_saved = input('What portion of your salary do you wish to save? ')
annual_salary = input('What is your starting annual salary? ')

def house_hunting(total_cost,portion_saved,annual_salary,r):
    
    current_savings = 0
    count = 0
    portion_down_payment = 0.25*int(total_cost)
    
    while current_savings < portion_down_payment:
        current_savings += float(portion_saved)*float(annual_salary)/12 + float(current_savings)*float(r)/12
        count = count + 1
        
    return print(count)

house_hunting(total_cost,portion_saved,annual_salary,0.04)

# Part B

total_cost = input('What is the cost of your dream home? ')
portion_saved = input('What portion of your salary do you wish to save? ')
annual_salary = input('What is your starting annual salary? ')
semi_annual_raise = input('What is your semi-annual raise? ')

def house_hunting(total_cost,portion_saved,annual_salary,semi_annual_raise,r):
    
    current_savings = 0
    count = 0
    portion_down_payment = 0.25*int(total_cost)
    annual_salary = float(annual_salary)
    
    while current_savings < portion_down_payment:
        if count%6 == 0 and count > 0:
            annual_salary += float(annual_salary) * float(semi_annual_raise)
        current_savings += float(portion_saved)*float(annual_salary)/12 + float(current_savings)*float(r)/12
        count = count + 1
        
    return print(count)

house_hunting(total_cost,portion_saved,annual_salary,semi_annual_raise,0.04)

# Part C

down_payment = 0.25
total_cost = 1000000
down_payment_cost = down_payment * total_cost
semi_annual_raise = 0.07
r = 0.04

def best_savings_rate(starting_salary):
    high = 0.8
    low = 0.2
    portion_saved = (high+low)/2
    current_savings = 0
    bisection_count = 0
    while abs(down_payment_cost - current_savings) >= 100:
        current_savings = 0
        annual_salary = starting_salary
        for count in range(0,37):
            if count%6 == 0 and count > 0:
                annual_salary += float(annual_salary) * float(semi_annual_raise)
            current_savings += float(portion_saved)*float(annual_salary)/12 + float(current_savings)*float(r)/12
        if current_savings < down_payment_cost:
            low = portion_saved
        else:
            high = portion_saved
        annual_salary = starting_salary
        portion_saved = (high + low)/2
        bisection_count += 1
        
    return print(portion_saved,bisection_count)

best_savings_rate(150000)
            



        
    
    
    