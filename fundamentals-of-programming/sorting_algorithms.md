# Algorithms

## Bubblesort

This algorithm works by comparing adjacent elements and performing a swap to organise them in to ascending order, sweeping from left to right. The highest unsorted number will always find its way to the rightmost unsorted index; like a bubble, it raises to the top.

```python
for i in range(1,n):
    for j in range(i):
        if a[j] > a[j+1]:
	    swap(a[j],a[j+1])
```

Bubblesort is a very slow algorithm; it makes n-1 comparisons for the first element, n-2 for the second and so on. The number of comparisons is quadratic in n therfore, whcih we can say is big O of n squared.

## Insertion sort

Insertion sort is similiar to bubble sort. We insert an element at a time to an ever increasing sorted array. We then swap elements where necessary to ensure that the elements within that array are are ordered. Insertion sort takes advantage of presorting and as a result requires fewer comparisons than bubble sort.

```python
for i in range(1,n):
    j = 1:
        while j > 0 and a[j] < a[j-1]:
	     swap(a[j], a[j-1])
	     j = j - 1
```

## Merge sort

Merge sort takes two ordered sequences and merges them in to one sorted sequence. We compare the smallest elements of each array, one by one, and put the smallest one in to the sorted array. This process repeats until all the elements have been put in to the single combined array. For merge sort we need to create new arrays. To order 8 numbers we find that we require a total of 7 new arrays.

```python
for i in range(1,size):
    i = 2i
    for j in range(1,size):
        j = j + 2i:
        Merge(&a[j], i, min(2i, size - j))

Merge(A, end1, end2):
    i = 0, j = end1, k = 0:
    while i < end1 and j < end2:
        if a[i] < a[j]:
	    temp[k] = a[i]
	    i += 1
	    k += 1
	else:
	    temp[k] = a[j]
	    j += 1
	    k += 1
	while i < end1:
	    temp[k] = a[i]
	    i += 1
	    k += 1
	while j < end2:
	    temp[k] = a[j]
	    j += 1
	    k += 1
	for i in range(1, end2)
	    a[i] = temp[i]
```
## Quick sort

The quick sort alogorithm is performed by the repeated application of what's called a partition step. We start with an unordered array and select a pivot. We use the pivot to partition the array in to two sets; values larger than the pivot and values smaller than the pivot are separated either side of it. After partioning the origin array in to two sets we repeat the partition step on these smaller sub arrays.

```python
Partition(a, size):
    if size < 2:
        return
    pivot = a[np.random.randint(len(a))]
    l = 0, u = size - 1
    while l < u:
        while a[l] < pivot:
	    u += 1
	while a[u] > pivot
	    u -= 1
	Swap(a[l], a[u])
    Partition(a, l)
    Partition(&a[l+1], size-l-1)
```

## Selection sort

The selection sort algorithm loops over the elements of an array and stores the index of the array element with the smallest remaining integer. We can then swap this element in to position. The algorithm loops this process this until the array has been sorted.

## Linear and Binary search

For a binary search we require an ordered list whereas for a linear search we do not.

For a linear search we begin with an array of elements and a target element. Beginning at the first element we continually check each element for equality until we find it or otherwise run out of elements in the array.

```python
LinearSearch(A, size, target):
    for i in range(size):
        if a[i] == target:
	    return i
	return -1
```

If the array is already ordered we may use a binary search algorithm. We first choose the middle element to see if it is larger than the target - if it is we can eliminate the last half of the array. By repeatedly applying such comparisons we can continute to eliminate half of the remaining elements until we are left with the search target. Binary search approximately requires log(n) (of base 2) + 1 operations to sucessfully complete the search. For example, for an array of size 100 the average comparisons for linear search is 50.5, whereas for binary search it is 7.72. Binary search therefore represents a significant speed advantage.

```python
BinarySearch(a, size, target):
    low = 0
    high = size
    while low + 1 < high:
        test = (low + high) / 2
	if a[test] > target:
	    high = test
	else:
	     low = test
	 if a[low] == target:
	     return low
	 else:
	     return -1
```

## Measuring efficiency

When we evaluate an alogirthm's efficiency we need to know its scalability. We measure an algorithms speed with the growth of the number of operations relative to the input size.

* *Defintion of Big 0:* f(x) in O(g(x)) if there exists c, x0 > 0 such that f(x) <= c*g(x) for all x => x0.

* *Definition of Big Omega:* f(x) in Omega(g(x)) if there exists c, x0 => 0 such that f(x) => c*g(x) for all x => x0.

* *Definition of Big theta:* f(x) in Theta(g(x)) if there exist c1, c2, x0 > 0 such that c1*g(x) => f(x) => c2*g(x) for all x => x0.

## Heap

A heap is a kind of tree where a parent node is connected to two daughter nodes with the only requirment that the parent node has a larger value than both of its daugter nodes. To remove a node we remove the root of the tree then do daughter/parent swaps to re-establish our heap.

We can use heaps to sort an array of elements. We first create a heap and then use its properties to sort the elements. Creating a heap involves adding a single element to the heap at a time. We may need to swap introduced elements as we add them to the heap. The heap sort algorithm simply involves moving the root element (i.e. the largest value left within the tree) and swapping it with the last element of the heap. We then need to put the element moved to the root back in its proper place by using the parent/daughter swap algorithms. When we do the intial swap the root element which gets swapped is simply put to the end of the heap and is "sorted".

