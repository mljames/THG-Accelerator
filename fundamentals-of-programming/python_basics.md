# Python Basics

## Why do we need python?

We can use python for two things. The first is for programming "in-the-small" - we need python to write code to do simple, well-defined tasks. We can then join these modules to solve messy, large problems; this is referred to as programming "in-the-large". Programming is important for *abstraction*; it allows us to utilise the hardware of a computer in ways we don't understand through simple commands.                                                                                 

We can code a computer to perform any task we wish using "if", "while", "for" loops in what are called "branching programs". Combined with the use of algorithms we can perform many tasks with great efficiency, e.g. Heron's Square Root, Bisection search, Newton-Raphson etc. Programs may finally become incredibly sophisticated once "functions" and "classes" are introduced (the implementation of these ideas are all included within the various programming exercises performed).        

## Useful operations

* i == j
* i != j
* i > j
* i < j
* i >= j
* i <= j
* i + j
* i - j
* i // j
* i / j
* i % j

## Types

* list - [] such as an array, completely mutable.
* range - range(1)
* tuple - () where a tuple with 1 entry written as (1,) ; can also have tuples holding tuples, essentially a tuple is an object that can store other objects, completely immutable.
* dict - {} then we can put things in it e.g. cars = {} cars['phil'] = 'BMW 3-Series' then to test print(repr(cars)), mutable in the sense we can add things to the dictionary - best to use immutable objects however for the key which will mean the hashing cannot change. Defining a non empty dictionary: {'first': 'phil', 'last': 'wilson', 'shoe size': 9}
* int - 1
* float - 1.0
* str - 'one'
* sets - `empty = set()`, `workdays = set(['monday', 'tuesday', etc. ])`, we can iterate over them e.g. `for day in workdays:` or search within them `workdays.__contains__('monday')` and we can calculate the length e.g. `len(workdays)`. They also obey mathematical set operations, such as the union `british | french` or `british.__or__(french)` or the intersection `britsh & blue` or `british..__and__(blue)`, or an or without the and `british ^ blue)` or ``british.__xor__(blue)`. We may also remove easily `french.remove('camembert')`.

Some are mutable, such as lists, whereas others are not. Mutability and immutability are relevant in different situations.

Classes are a way of defining **abstract** types. In the same way that definitions produce functions which do stuff" for our very specific needs, classes define objects of a particular type, that are precise to our needs and are therefore non-standard (as are ints, floats, lists etc.).

### List comprehensions

We can print values straight in to a list like:

```python
def square(x):
    return x*x
squares = [square(x) for x in range(11) if x%2 == 0]
```

And for nested lists we have:

```python
colours = ('black','white','red')
size = ('S','M','L','XL')
[(colour, size) for colour in colours for size in sizes]
```
Generator expressions have similiar syntax but return generator objects such as range().

## Exceptions and Insertions

**Syntax errors** - gramatically incorrect code caught at compile time

**Run-time errors** - python encounters something it can't handle

Exceptions should be caught and handled using *try* and *except* clauses:

```python
while True:
    val = input('Please input an integer')
    try:
        val = int(val)
	print(val, 'squared is', val*val)
	break
    except ValueError:
        print(val, 'is not an integer')   
```

By default print() prints to std.out but we can push errors to a standard error output by the option argument "print(val,file=sys.stderr)". Now any exception error will be pushed away from desired output which is printed in std.out. Can also include a sys.argv[0] option argument, which can track from which file the error message appeared, for example "print(sys.argv[0], val, file=sys.stderr)".

Built-in exceptions include:

* ZeroDivisionError
* AssertionError
* ImportError
* IndexError
* KeyboardInterrupt
* KeyError
* NameError
* TypeError
* ValueError
* IOError

We can throw exceptions with raise():

```python
except Exception as e:
    do something
    raise(e)
```

Where the raise(e) is written to reraise the current exception in an exception handler so that is can b handled to a shallower part of the call stack.

## Object Oriented Programming

### Introduction

Classes are Python's mechanism for making new types. A class definition creates an object of type *type* and associates instance methods with that class. We are essentially expanding beyond the builtin library of types already available such as *int*, *float*, *list*, *tuple* etc.

* Methods - function definitions inside classes
* Instantiation (construction) - create a new object of the class type
* Attribute reference - dot notation to access methods

When an object is created that's called **instantiation** creating an **instance** of the class - hence "instance methods" and "instance variables". Instance methods are like functions with a special first formal parameter (*self*). When we call an instance method through an object using dot notation, the object is passed to the instance method as the first formal parameter.

For example:

```python
from math import gcd

class Rational:
    def __init__(self, numerator, denominator):
        if (denominator < 0):
              numerator, denominator = numerator*-1, denominator*-1
        g = gcd(numerator, denominator)
        self.numerator = numerator // g
        self.denominator = denominator // g
    def string(self):
        if self.denominator == 1:
            return str(self.numerator)
        else:
	    return "{0}/{1}".format(self.numerator, self.denominator)
    def add(self, a):
        n = self.numerator * a.denominator + self.denominator * a.numerator
        d = self.denominator * a.denominator
        return Rational(n, d)

a = Rational(3, 4) # __init__ method means primary function when Rational called
b = Rational(1, 2)
```

***Caveat*** - Why is a.denominator (an instance variable) discouraged from referencing outside of the class? We should actually build a method to return the instance variable rather than access the variable directly (from outside the class), this is called a *getter* method. Getter methods do not change the values of atrributes, they just return the values. Accessing instance variables directly allows us to change their values from outside of the class, which is **bad**. The converse to these getter methods are called *setter* methods, and are methods of a class that allow us to set instance variables from outside of the class. Getter and setters are used in many other object oriented programming languages, always with the principle of ensuring data encapsulation.

### Special methods

There are other special methods than just the __init__ within classes:

* __str__(self) and return str(a) we get back '3/4' and print(a) gives 3/4 which also calls the __str__ method

* __add__(self, a) and we can get print(a + b) and we will get back 5/4

For a full list of these special methods follow https://docs.python.org/3/reference/datamodel.html.

### Hiding instance variables

We can us `__name` (using dunders) to make an instance variable invisible to interrogation. The way this works is that the class object changes te name of the instance variable in the key/value pair of a dictionary. If we know the name it has been changed to we *can* go in and change it.

### Function Decorators

* Functions can be input to other functions.
* Functions can return other functions.
* Functions can be values in a dictionary.
* If a function is in the lexical scope of another it can inherit values and return based on the input of that other function, but at a later time.

This last point is probably the most interesting and most difficult to get the head around! For example:

```python
def make_multiplier_of(n):
    def multiplier(x):
        return x * n
    return multiplier

times3 = make_multiplier_of(3)
print(times3(9))

>> 27
```

This short script includes a function that returns a function. So when we call the outer function we are returning the inner function and assigning that to a variable. We can then use that variable as a method to execute the inner function.

Another example would be:

```python
def count_invocations(func):
    counter = [0]
    def wrapper_func(*args, **kwargs):
        counter[0] += 1
	print(counter[0], 'invocations')
        return func(*args, **kwargs)
    return wrapper_func

f = count_invocations(greeting)
f('phil')
f('phil')

>> 1 invocations
>> 2 invocations
```
The counter remains defined because we remain within the lexical scope of the outer function when we execute the function `f` over and over because of the assignment `f = count_invocations(greeting)`, where `count_invocations(greeting)` provides the environment in which `counter[0]` is stored.

In this spirit, a decorator function is a function that takes a function and augments that function in some way - as above we can use instead:

```python
@count_invocations
def greeting(name):
    return "hello, " + name

greeting('fred')

>> 1 invocations (as before)
```

Calling `greeting('fred')` with the decorator defined as `@count_invocations` (the function above) provides a short hand way to do the same thing, i.e. `f = count_invocations(greeting)` followed by `f('fred')`.  A decorator in Python is any callable Python object that is used to modify a function or a class, as we can see. A reference to a function, "func", or a class "C" is passed to a decorator and the decorator returns a *modified* function or class. The decoration happens in the example above at the stage `f = count_invocations(greeting)` where f points to the `wrapper_func` inner function. Using decorator functions we can perform tasks we want (i.e. functions within functions) in a nice syntactic way.  

Class and static methods both use decorator functions. Class methods take `cls` rather than `self` as their first argument. A static method is just a function without 'self' or 'cls' (useful for utility or helper functions). We can also use a method called `@property` and a method called `@name.setter` to use function decorators for getter and setter methods, which were mentioned above.

## Algorithms and Complexity

Big O notation:

* f(x) = O(g(x)) as x tends to infinity

When estimating time complexity we generally drop constants and multiplicative factors - therefore focus on dominant terms. Big O notation satisfies the law of addition, where we can apply some rules and focus on the dominant terms. Similiarly they are multiplicative. Complexity classes include:

* O(1) - constant
* O(log(n)) - logarithmic
* O(n) - linear
* O(nlog(n)) - log linear
* O(n**c) - polynomial
* O(c**n) - exponential

### Search alogorithms

* Linear search - O(n) however the list does not have to be sorted
* Binary (bisection) search - O(log(n)) however the list must be sorted

This raises the question of when it makes sense to sort a list - we must amortize cost of sorting across many searches. Since we cannot ignore any element in a sorting algorithm, all sorting algorithms must have complexity of at least O(n). Some examples of sorting algorithms include:

* Bubble sort - compare consecutive pairs and swap elements in pairs so smaller is first until sorted
* Selection sort - find the smallest element  and then swap it with the element at index 0 until sorted
* Merge sort - use a divide and conquer approach; if a list has more than one element split into 2 lists until these lists have length one - then combine elements knowing that within each sub list we have ordered elements - requires only one sweep through each sub list.

## Common Data Structures

### List

* A finite sequence of zero or more elements.
* Can represent vectors
* Have length
* First element called head, the remainder the tail

Operations we can support on lists include *insertion, deletion and lookup*.

A **linked list** includes a head which holds two values: the value of the head and the position of the next element in the list. This next node has the same construction which continues until the pointer points to a terminating sentinel.

### Stack

Stacks are derived from list - fundamentally stacks provide for two main operations *push* and *pop*, where push adds an element to the stack and pop removes an element from the stack, where this element is always at the top. To this end the time complexity is constant.

### Queues

The entities in the collection are kept in order and the only operations on the collection are the addition of entities to the rear terminal, an *enqueue*, or the removal of entities from the front terminal position, a *dequeue*.

Queues are relevant to the producer-consumer problem, which is a classic example of a multi-processing synchronisation problem. The problem describes two processes, the producer and the consumer, who share a common, fixed-size buffer used as a queue.

```Python
class Node():
  def __init__(self,previous=None,next=None,contents=None):
    self.previous = previous
    self.next = next
    self.contents = contents
  def __repr__(self):
    return "Node(contents={0})".format(self.contents)

  class Queue():
    def __init(self):
      self.front = None
      self.rear = None
    def empty(self):
      return self.front == None
    def enqueue(self,datum):
      node = Node(contents = datum)
      if self.empty():
        self.front = node
      else:
        node.next = self.rear
        node.next.previous = node
      self.rear = node
    def dequeue(self):
      if self.empty():
        return None
      contents = self.front.contents
      self.front = self.front.previous
      if self.front:
        self.front.next = None
      return contents
    def __iter__(self):
      n = self.rear
      while(n):
        yield n.contents
        n = n.next
    def __str__(self):
      return ', '.join([str(x) for x in self 
```
