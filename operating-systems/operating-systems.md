# Operating Systems

Resources:

1. The Unix Programming Environment
2. Advanced Programming in the UNIX Environment
3. www.cl.cam.ac.uk/teaching/1718/OpSystems/

## Bash

Useful bash commands:

* wc
* grep
* sort
* uniq
* tr
* sed
* bc 

Tools for looking inside files:

* less
* more
* cat
* head
* tail

Tools for creating, copying and removing files and directories

* touch
* cp
* mv
* ln
* rm
* mkdir
* rmdir
* find
* locate

Tools for editing files:

* tar
* gzip
* zcat
* bzip2

Other useful stuff:

* echo - to test
* "*" - wildcard, which can substitute any string
* man - calls a help page for any command that follows
* "?" - wildcard for any single character
* "`command`" - takes standard output and puts it on the command line
* which *executable* - finds path for an executable

Note we can append things to a ~/.bash_profile or ~/.bashrc file such as path variables by export PATH=$PATH:~/thing/to/bash or PATH=~/thing/to/bash:$PATH whether we want that directory to be the first or last place the shell looks for a certain command. For example if we have different versions of python we may want the python command within a terminal to point a particular version, which may be installed in a particular environment. Type ** man bash ** for more information on the bash terminal.

We can create things like pipelines using the commands given above. The examples used in lectures include manipulation of the Shakespeare text retrieved from:

```python
curl http://inst.eecs.berkeley.edu/~cs61a/fall/shakespeare.txt > shakespeare.txt
```

We may chop it with some command line tools:

```python
sed 's/\s/\n/g' shakespeare.txt | sed 's/[^A-Za-z]//g' | tr A-Z a-z | sort | uniq > shake_words.txt
```

## Operating System Architecture (OS)

An OS is something that manages hardware and other software; it provides common services for other programs - including the programs that we may create. The behaviour of a particular program may only be understood by understanding how an OS works. The OS is rather like a house which we live in when we program. It includes:

* File storage
* Users and permissions
* Network and other communications
* A vast toolbox for programming

An OS is made up of a kernal, libraries, a shell and other user interfaces as well as a variety of utilites.

The OS importantly handles login; when we try to login the computer looks into the files /etc/password and /etc/shadow with the latter requiring super user permissions - these files contain encryption keys. /etc/profile configures the environment for all users - for example it sets up the "profile $" prompt on a bash shell. However, a ~/bash_profile is a personal set of instructions for a particular user; here one can export paths, point to particular environments, define editor preferences (e.g. export EDITOR = emacs), etc.

## Data

* Bit - binary digit; the basic unit in digital computing and communications and can be in one of two states, 0 or 1.
* Byte - an 8-binary digit; 00000000-11111111. The byte value is often referred to as two hexadecimal (2^4 = 16 distinct values) digits - each hex digit can represent 4 bits, i.e. half of a byte. Binary literals are prefixed by 0b. Hex literals are prefixed by 0x. A byte has a maximum value of 255, i.e. 0b11111111. A hex can be represented by 0123456789ABCDEF.
* (Machine) Word - The number of bits that can be stored in a register on a particular processor - generally 64 bits.

For a byte:

```python
>>> num = 0b10111101
>>> num
```

Where num evaluates to 189. 

For a hex:

```python
>>> num = 0xBD
>>> num
```

Where num evaluates to 189.

* kilobyte - thousand (k)
* mega - million (M)
* giga - billion (G)
* tera - trillion (T)
* peta - quadrillion (P)

**N.B:** b refers to bits, B refers to bytes.

Because a byte can hold 256 different values (0-255) we can only represent 256 characters in this one-to-one construction. Historically we initially said that each distinct value corresponds to a character using only 7 bits (ASCII), with the spare bit used as a parity check.

We can interrogate all of this using the following commands:

```python
>>> chr(0b1011101)
```

represents the character ']'. Going the other way:

```python
>>> ord('A')
```

is represented by the byte value 65.

The 8th bit within a byte is used as a parity check, that tells us whether there are even or odd number of 1 bits in a string - this can be used for protection against eavesdropping. ASCII solves only America's problem for representing characters, what about other languages that use different characters, such as Chinese? There are diverse alternate shcemes for character sets with more than 256 characters which can no longer be represented by the possible values of a single byte. **Unicode** was developed to resolve the chaos of multiple encoding schemes. Unicode defines 137439 characters in 146 modern and historic scripts - each character has a corresponding "code point" which is an integer between 0x000000 and 0x10FFFF; we have used 10% of possible code points. UTF-8 is a variable width character encoding capable of encoding all of the valid code points in Unicode using one to four 8-bit bytes, but named as it uses a miniumum of 8 bits i.e. 1 byte. UTF-16 on the other hand uses a minimum of 16 bits or 2 bytes to encode the unicode characters and is used in JAVA.

## Numbers

### Integers

The largest integer we can store in a 64 bit quantity is equal to 2^64 - 1. In actual fact taking in to account negative numbers the highest positve integer is equal to 2^63 - 1 where the value in the first bit tells the computer that the following number is negative, or not. If the highest bit is 0 the number is positive, if the highest bit is 1 the number is negative.

Microprocessers keep track of the overflow. In the case of 0b11111111, the largest 8 bit number we can produce, adding 1 to this gives us 0b100000000, which within the first 8 bits actually gives us the smallest 8 bit number we can make, 00000000. The overflow can alert us to errors created by byte capacities by storing the value of the next bit. We can however not do arithmetic using this overflow bit, it is merely used a check.

### Floating point numbers

For an introduction on floating point numbers, the following is a useful resource:

https://www.cl.cam.ac.uk/teaching/1011/FPComp/fpcomp10slides.pdf

Scientific notation written as: mantissa * (base ** exponent)

If we have 64 bits how do we represent a floating point number in scietific notation? There is now a standard:

* 1 bit is for sign - 0 positive, 1 negative
* 52 bits for the mantissa (which includes 51 bits of fractional part and first bit must be a 1 e.g. 1.111111)
* 11 for exponent (which also needs to be signed)

Largest floating point number is equal to ~ 2 * (2^(2^10)) which is much bigger than our largest integer. The basic problem of floating point numbers is:

**The set A of all possible representable numbers on a given machine is finite - but we would like to use this set to perform standard arithmetic operations (+,*,-,/) on an infinite set. The usual algebra rules are no longer satisfied since results of operations are rounded.** 

Thus for example: a + (b + c) != (a + b) + c

**fl(x)** notates the closest floating point representation of a real number x ('rounding'). When a number x is very small, there is a point when 1 + x == 1 in a machine sense. The computer no longer makes a difference between 1 and 1 + x. Machine epsilon is defined as:

**Machine epsilon: The smallest number, n, such that 1 + n is a float that is different from one, is called machine epsilon. Denoted by macheps or eps, it represents the distance from 1 to the next larger floating point number.**

eps = B^-(t-1) and so for double precision, B = 2 and t = 53 (includes 'hidden bit') and therefore eps = 2^-52.

Thus when we want to "do stuff" with say 0.1, this gets transferred to a machine representation that may be represented in the exponent form given above in such a way that is not exactly equal to 0.1 (but is quite close to it). This causes rounding erros.

We saw in the lecture also that:

1x10^20 - (1x10^20 - 1) = 0.0

This is because the machine epsilon error gets so big for larger numbers that the computer can only represent the two numbers on the left hand side as the same machine binary representation (after rounding). Thus the answer will be 0 because it is essentially subtracting the same number from the other. The precision of floating point numbers is constrained by the fact we have only 52 bits for the mantissa - the larger the numbers get the larger the spacing between available floating point representations on the number line become. 

For a great explanation of the arithmetic of gap size then follow: https://www.exploringbinary.com/the-spacing-of-binary-floating-point-numbers/. Essentially the gap size is given by the formula:

**gap size = 2^(e+1-p)**

where e is the exponent and p is the precision. For double precision p will equal 53 and the e could range between -1022 to 1023 (where double precision refers to 64-bits).

To hammer the point home therefore, this gap size makes binary floating point numbers discontinous. Precision determines the number of gaps and precision and the exponent together determine the size of the gaps. The spacing therefore explains why conversions between binary and decimal floating point numbers round-trip only under certain conditions and why numbers of wildly different magnitudes don't mix. The gaps start getting huge. In the neighbourhood of 1e300, which in double pecision converts to 1.011111100100001111001000100000000000011101011001111 x 2**996 the gap is a whopping 2^944. This explains why adding 1, 1000 or even a really massive number like 2^943 to 1e300 still gives us 1e300 after rounding back to 53 bits. 
